<?php
require "../config/conexion.php";
class Mensaje
{
    public function __construct()
    {
    }
    public function crear_mensaje($msj)
    {
        $message = "";
        try {
            $sql = "INSERT INTO mensaje (mensaje) VALUES ('$msj')";
            $data = ejecutarConsulta($sql);
            if ($data) {
                $message = "success";
            } else {
                $message = "INSERT INTO mensaje (mensaje) VALUES ('$msj')";
            }
            return ["info" => $data, "message" => $message];
        } catch (\Throwable $th) {
            $message = "INSERT INTO mensaje (mensaje) VALUES ('$msj')";
            return ["info" => [], "message" => $message];
        }
    }
    public function editar_mensaje($id_mensaje, $msj)
    {
        $message = "";
        try {
            $sql = "UPDATE mensaje SET mensaje='$msj' WHERE id_mensaje=$id_mensaje";
            $data = ejecutarConsulta($sql);
            if ($data) {
                $message = "success";
            } else {
                $message = "UPDATE mensaje SET mensaje='$msj' WHERE id_mensaje=$id_mensaje";
            }
            return ["info" => $data, "message" => $message];
        } catch (\Throwable $th) {
            $message = $th;
            return ["info" => [], "message" => $message];
        }
    }
    public function mostrar_mensaje()
    {
        $sql = "SELECT * FROM mensaje";
        return ejecutarConsultaSimpleFila($sql);
    }
    public function obtener_registrado($id = 0)
    {
        $sql = "";
        if ($id == 0) {
            $sql = "SELECT id_memsaje, mensaje FROM mensaje ORDER BY id_memsaje DESC LIMIT 1";
        } else {
            $sql = "SELECT id_memsaje, mensaje FROM mensaje AND id_memsaje = '$id' LIMIT 1";
        }
        return ejecutarConsulta($sql);
    }
}
