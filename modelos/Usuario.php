<?php
require "../config/conexion.php";

class Usuario
{
    public function __construct()
    {
    }

    public function insertar(
        $nombre,
        $user,
        $clavehash,
        $sexo,
        $telefono,
        $foto,
        $cargo,
        $fecha_registro,
        $fecha_actualizacion,
        $fecha_ultimo_ingreso
    ) {
        $sql = "INSERT INTO usuario (nombre,user,password,sexo,telefono,
        foto,cargo,fecha_registro,fecha_actualizacion,fecha_ultimo_ingreso,activo)
        VALUES ('$nombre','$user','$clavehash','$sexo','$telefono',
        '$foto','$cargo','$fecha_registro', ','$fecha_actualizacion'','$fecha_ultimo_ingreso','1')";
        //return ejecutarConsulta($sql);
        return ejecutarConsulta($sql);
    }

    public function editar(
        $id_usuarios,
        $nombre,
        $user,
        $clavehash,
        $sexo,
        $telefono,
        $foto,
        $cargo,
        $fecha_registro,
        $fecha_actualizacion,
        $fecha_ultimo_ingreso,
        $activo
    ) {
        $sql = "UPDATE usuario SET nombre='$nombre', user='$user',password='$clavehash',
        sexo='$sexo', telefono='$telefono',foto='$foto',cargo='$cargo',
        fecha_registro='$fecha_registro', fecha_actualizacion='$fecha_actualizacion', fecha_ultimo_ingreso='$fecha_ultimo_ingreso', activo='$activo'
        WHERE id_usuarios='$id_usuarios'";
        return ejecutarConsulta($sql);
    }

    public function desactivar($id_usuarios)
    {
        $sql = "UPDATE usuario SET activo='0'
        WHERE id_usuarios='$id_usuarios'";
        return ejecutarConsulta($sql);
    }

    public function activar($id_usuarios)
    {
        $sql = "UPDATE usuario SET activo='1'
        WHERE id_usuarios='$id_usuarios'";
        return ejecutarConsulta($sql);
    }

    public function mostrar($id_usuarios)
    {
        $sql = "SELECT * FROM usuario WHERE id_usuarios='$id_usuarios'";
        return ejecutarConsultaSimpleFila($sql);
    }
    public function listar()
    {
        $message = "";
        try {
            $sql = "SELECT * FROM usuario";
            $data = ejecutarConsulta($sql);
            if( $data ) { $message = "success"; } else { echo "empty - SQL"; }
        } catch (\Throwable $th) {
            $message = $th;
        }
        return [ "info" => $data, "message" => $message ];
    }
    public function listamarcados($id_usuarios)
    {
        $sql = "SELECT id_rol_usuario FROM usuario_permiso WHERE id_usuario=$id_usuarios AND activo=1";
        return ejecutarConsulta($sql);
    }
    public function verificar($login, $clave)
    {
        $message = "";
        try {
            $sql = "SELECT id_usuarios,nombre, user, sexo, telefono, foto, cargo FROM usuarios WHERE user='$login' AND password='$clave'
            AND activo='1' LIMIT 1";
            $data = ejecutarConsultaSimpleFila($sql);
            if($data) { $message = "success"; } else { $message = "error SQL"; }
        } catch (\Throwable $th) {
            $message = $th;
        }
        return ["info" => $data, "message" => $message];
    }
    public function cambiar_contraseña($id_usuarios, $password)
    {
        $sql = "UPDATE usuario SET password='$password' WHERE id_usuarios='$id_usuarios'";
        return ejecutarConsulta($sql);
    }
}
