<?php
require "../config/conexion.php";

class Gafas
{
    public function __construct()
    {
    }
    public function getId()
    {
        $sql = "SELECT idGafas FROM Gafas ORDER BY idGafas  DESC";
        return ejecutarConsultaSimpleFila($sql);
    }
    public function insertar($marca, $modelo, $color, $articulo, $cantidad, $genero, $precioUno, $precioDos)
    {
        // $id, $id,
        $sql = "INSERT INTO Gafas(marca, modelo, color, articulo, cantidad, genero, precioUno, precioDos) VALUES('$marca', '$modelo', '$color', '$articulo',$cantidad, '$genero', $precioUno, $precioDos)";
        echo "INSERT INTO Gafas(marca, modelo, color, articulo, cantidad, genero, precioUno, precioDos) VALUES('$marca', '$modelo', '$color', '$articulo',$cantidad, '$genero', $precioUno, $precioDos)";
        return ejecutarAccion($sql);
    }

    public function editar($id, $marca, $modelo, $color, $articulo, $cantidad, $sexo, $precioUno, $precioDos)
    {
        $sql = "UPDATE Gafas SET marca='$marca', modelo='$modelo', color='$color', articulo='$articulo', cantidad = '$cantidad', genero='$sexo', precioUno='$precioUno', precioDos='$precioDos' WHERE idGafas = '$id'";
        echo "UPDATE Gafas SET marca='$marca', modelo='$modelo', color='$color', articulo='$articulo', cantidad = '$cantidad', genero='$sexo', precioUno='$precioUno', precioDos='$precioDos' WHERE idGafas = '$id'";
        return ejecutarAccion($sql);
    }

    public function obtener_registrado($id = 0)
    {
        $sql = "";
        if ($id == 0) {
            $sql = "SELECT idGafas, marca, modelo, color, articulo, cantidad FROM Gafas ORDER BY idGafas DESC";
        } else {
            $sql = "SELECT idGafas, marca, modelo, color, articulo, cantidad FROM Gafas WHERE idGafas = '$id'";
        }
        return ejecutarConsultaSimpleFila($sql);
    }

    public function activar_desactivar($id, $opcion)
    {
        $sql = "UPDATE Gafas SET habilitado = '$opcion' WHERE idGafas = '$id'";
        return ejecutarAccion($sql);
    }

    public function obtener_data($id)
    {
        $sql = "SELECT * FROM Gafas WHERE idGafas = $id";
        return ejecutarConsulta($sql);
    }

    public function listar($pagina)
    {
        $registrosPorPagina = 10;
        $offset = ($pagina - 1) * $registrosPorPagina;
        $sql = "SELECT idGafas
            ,marca
            ,modelo
            ,color
            ,articulo
            ,cantidad
            ,genero
            ,fechaRegistro
            ,precioUno
            ,precioDos
        FROM Gafas ORDER BY idGafas DESC
        OFFSET $offset ROWS
        FETCH NEXT $registrosPorPagina ROWS ONLY";
        // echo json_encode($sql);
        $data = ejecutarConsulta($sql);
        $sql2 = "SELECT COUNT(*) AS numero_de_registros
        FROM Gafas";
        $length = ejecutarConsulta($sql2);
        return ["tamanio" => $length, "datos" => $data];
    }

    public function eliminar($id)
    {
        $sql = "DELETE FROM Gafas WHERE idGafas='$id'";
        echo $sql;
        return ejecutarAccion($sql);
    }
}
