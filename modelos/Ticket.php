<?php
require "../config/conexion.php";

class Ticket
{
    public function __construct()
    {
    }
    public function insertar(
        $codigo_ticket,
        $fecha,
        $hora,
        $id_conf_tipo_colas
    ) {
        $sql = "INSERT INTO ticket (codigo_ticket, fecha, hora, id_conf_tipo_colas, estado_ticket, activo) VALUES('$codigo_ticket','$fecha','$hora','$id_conf_tipo_colas','pendiente',1)";
        return ejecutarAccion($sql);
    }
    public function estado($id_ticket)
    {
        $sql = "UPDATE ticket SET estado_ticket='atendido' WHERE id_ticket='$id_ticket'";
        return ejecutarConsulta($sql);
    }
    public function listar()
    {
        $sql = "SELECT * FROM ticket";
        return ejecutarConsulta($sql);
    }

    public function verificar_ticket($fecha_actual)
    {
        $sql = "SELECT EXISTS(
             SELECT fecha
             FROM ticket as t
            WHERE t.fecha = '$fecha_actual') AS verificado";
        return ejecutarConsultaSimpleFila($sql);
    }
}
