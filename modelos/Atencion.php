<?php
require "../config/conexion.php";

class Atencion
{
    public function __construct()
    {
    }
    public function insertar(
        $codigo_ticket,
        $fecha,
        $hora,
        $id_conf_tipo_colas,
        $estado_ticket,
        $veces_llamado,
        $activo
    ){
        $sql = "INSERT INTO ticket (codigo_ticket, fecha, hora, id_conf_tipo_colas, estado_ticket, veces_llamado, activo) VALUES('$codigo_ticket','$fecha','$hora','$id_conf_tipo_colas','$estado_ticket', 0,'$activo')";
        $valor = ejecutarConsulta($sql);
        return $valor;
    }
    public function estado($id_ticket)
    {
        $sql = "UPDATE ticket SET estado_ticket='atendido' WHERE id_ticket='$id_ticket'";
        return ejecutarConsulta($sql); 
    }
    public function listar()
    {
        $sql = "SELECT * FROM ticket";
        return ejecutarConsulta($sql);
    }
    public function llamado($id_ticket)
    {
        $consultar = " ";
        $sql = "UPDATE ticket SET veces_llamado='atendido' WHERE id_ticket='$id_ticket'";
        return ejecutarConsulta($sql); 
    }
    public function solicitudes($fecha)
    {
        $sql = "SELECT * FROM ticket WHERE (estado_ticket = 'pendiente' OR estado_ticket = 'llamando' OR estado_ticket = 'atendiendo') AND activo='1' AND fecha = '$fecha' order by hora  limit 8";
        return ejecutarConsulta($sql);
    }
}
