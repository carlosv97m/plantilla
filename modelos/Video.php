<?php
require "../config/conexion.php";

class Video
{
    public function __construct()
    {
    }
    public function insertar($nombre, $link)
    {
        $sql = "INSERT INTO videos(nombre,link) VALUES('$nombre','$link')";
        return ejecutarAccion($sql);
    }

    public function editar($id, $nombre, $link)
    {
        $sql = "UPDATE videos SET nombre = '$nombre', link = '$link' WHERE id_video = '$id'";
        return ejecutarAccion($sql);
    }

    public function obtener_registrado($id = 0)
    {
        $sql = "";
        if ($id == 0) {
            $sql = "SELECT id_video, nombre, link, habilitado FROM videos WHERE activo = '1' ORDER BY id_video DESC LIMIT 1";
        } else {
            $sql = "SELECT id_video, nombre, link, habilitado FROM videos WHERE activo = '1' AND id_video = '$id' LIMIT 1";
        }
        return ejecutarConsultaSimpleFila($sql);
    }

    public function activar_desactivar($id, $opcion)
    {
        $sql = "UPDATE videos SET habilitado = '$opcion' WHERE id_video = '$id'";
        return ejecutarAccion($sql);
    }

    public function obtener_links()
    {
        $sql = "SELECT link FROM videos WHERE habilitado ='1' AND activo ='1'";
        return ejecutarConsulta($sql);
    }

    public function listar()
    {
        $message= "";
        try {
            $sql = "SELECT id_video, nombre, link, habilitado FROM videos WHERE activo = '1'";
            $data = ejecutarConsulta($sql);
            if( $data ) { $message = "success"; } else { $message = "SQL Error"; }
        } catch (\Throwable $th) {
            $message = $th;
        }

        return ["info" => $data, "message" => $message];
    }

    public function eliminar($id)
    {
        $sql = "UPDATE videos SET activo ='0' WHERE id_video = '$id'";
        return ejecutarAccion($sql);
    }
}
