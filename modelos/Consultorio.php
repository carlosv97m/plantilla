<?php
require "../config/conexion.php";

class Consultorio
{
    public function __construct()
    {
    }

    public function listar_pendientes($id_cola, $fecha)
    {
        $sql = "SELECT id_ticket, codigo_ticket, estado_ticket,veces_llamado FROM ticket WHERE fecha = '$fecha' AND
         (estado_ticket= 'pendiente' OR estado_ticket='llamando' OR estado_ticket='atendiendo')
        AND id_conf_tipo_colas = '$id_cola' AND activo='1' ORDER BY hora ASC LIMIT 2";
        return ejecutarConsulta($sql);
    }

    public function cambiar_estado($id_ticket, $estado, $veces_llamado)
    {
        $sql = "UPDATE ticket SET estado_ticket = '$estado', veces_llamado = $veces_llamado WHERE id_ticket ='$id_ticket' AND activo ='1'";
        return ejecutarAccion($sql);
    }

    public function contar_cola($id_cola, $fecha)
    {
        $sql = "SELECT COUNT(id_ticket) as num_pacientes FROM ticket WHERE fecha = '$fecha' AND
        (estado_ticket= 'pendiente' OR estado_ticket='llamando' OR estado_ticket='atendiendo')
        AND id_conf_tipo_colas = '$id_cola' AND activo='1'";
        return ejecutarConsultaSimpleFila($sql);
    }
}
