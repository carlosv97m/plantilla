create table if not exists usuarios (
    id_usuarios int not null auto_increment primary key,
    nombre varchar(100) not null,
    user varchar(25) not null,
    password varchar(230) not null,
    sexo varchar(9) not null,
    telefono int(8) not null,
    foto varchar(250),
    cargo varchar(15) NOT NULL,
    fecha_registro datetime not null,
    fecha_actualizacion datetime,
    fecha_ultimo_ingreso datetime,
    activo tinyint(2) not null default 1
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
create table if not exists consultorio (
    id_consultorio int not null auto_increment primary key,
    nombre_consultorio varchar(100) not null,
    tipo_consultorio varchar(100) not null,
    activo tinyint(2) not null default 1
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
create table if not exists asignacion_consultorio (
    id_asignacion_consultorio int not null auto_increment primary key,
    nombre_usuario varchar(100) not null,
    nombre_consultorio varchar(100) not null,
    fecha_asignacion datetime not null,
    id_usuarios int not null,
    id_consultorio int not null,
    estado_asignacion varchar(50) not null default 1,
    activo tinyint(2) not null default 1,
    foreign key(id_usuarios) references usuarios(id_usuarios),
    foreign key(id_consultorio) references consultorio(id_consultorio)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
create table if not exists conf_tipo_colas (
    id_conf_tipo_colas int not null auto_increment primary key,
    tipo_colas varchar(100) not null,
    prioridad_colas int(3) not null,
    activo tinyint(2) not null default 1
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
create table if not exists ticket (
    id_ticket int not null auto_increment primary key,
    codigo_ticket varchar(100) not null,
    fecha date not null,
    hora time not null,
    id_conf_tipo_colas int not null,
    estado_ticket varchar(50),
    veces_llamado int NOT NULL default 0,
    activo tinyint(2) not null default 1,
    foreign key(id_conf_tipo_colas) references conf_tipo_colas(id_conf_tipo_colas)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
create table if not exists atencion (
    id_atencion int not null auto_increment primary key,
    estado_atencion varchar(50) not null,
    fecha_atencion date not null,
    tiempo_atencion time,
    tiempo_de_atencion time,
    id_ticket int not null,
    id_asignacion_consultorio int,
    activo tinyint(2) not null default 1,
    foreign key(id_asignacion_consultorio) references asignacion_consultorio(id_asignacion_consultorio),
    foreign key(id_ticket) references ticket(id_ticket)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
create table if not exists videos(
    id_video int not null auto_increment primary key,
    nombre varchar(50) not null,
    link varchar(100) not null,
    habilitado tinyint(1) not null default 1,
    activo tinyint(1) not null default 1
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS mensaje (
	id_mensaje int not null auto_increment primary key,
    mensaje text NOT NULL
)ENGINE = InnoDB DEFAULT CHARSET = utf8;
