
-- TABLA SUARIOS --

INSERT INTO `usuarios` (`id_usuarios`, `nombre`, `user`, `password`, `sexo`, `telefono`, `foto`, `cargo`, `fecha_registro`, `fecha_actualizacion`, `fecha_ultimo_ingreso`, `activo`) VALUES
(1, 'Juan Perez', 'admin', '3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79', 'masculino', 12345678, NULL, 'admin', '2021-11-21 01:17:59', '2021-11-21 06:17:59', '2021-11-21 06:17:59', 1),
(2, 'Pablo Salinas', 'medico', '3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79', 'masculino', 88888888, NULL, 'medico', '2021-11-21 01:17:59', '2021-11-21 06:17:59', '2021-11-21 06:17:59', 1),
(3, 'Medico 1', 'fidelizado', '3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79', 'masculino', 88888888, NULL, 'fidelizado', '2021-11-21 01:17:59', '2021-11-21 06:17:59', '2021-11-21 06:17:59', 1),
(4, 'Medico 2', 'voluntario', '3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79', 'masculino', 88888888, NULL, 'voluntario', '2021-11-21 01:17:59', '2021-11-21 06:17:59', '2021-11-21 06:17:59', 1),
(5, 'Medico 3', 'distribucion', '3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79', 'masculino', 88888888, NULL, 'distribucion', '2021-11-21 01:17:59', '2021-11-21 06:17:59', '2021-11-21 06:17:59', 1),
(6, 'Sacar Tickets', 'ticket', '3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79', 'masculino', 88888888, NULL, 'ticket', '2021-11-21 01:17:59', '2021-11-21 06:17:59', '2021-11-21 06:17:59', 1),
(7, 'Vista Monitore', 'monitor', '3627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79', 'masculino', 88888888, NULL, 'monitor', '2021-11-21 01:17:59', '2021-11-21 06:17:59', '2021-11-21 06:17:59', 1);

INSERT INTO `conf_tipo_colas` (`id_conf_tipo_colas`,`tipo_colas`,`prioridad_colas`,`activo`) VALUES
(1,'fidelizado',1,1),
(2,'voluntario',0,1),
(3,'distribucion',0,1);

INSERT INTO `consultorio` (`id_consultorio`,`nombre_consultorio`,`tipo_consultorio`,`activo`) VALUES
(1,'consultorio 1','fidelizado',1),
(2,'consultorio 2','voluntario',1),
(3,'consultorio 3','distribucion',1);








/* --------------------------------
ESTADOS DE LA TABLA TICKET
PENDIENTE => Cuando se saca el ticket
LLAMANDO => Cuando el consultorio solicita un ticket
ATENDIENDO => Cuando el usuario llega a consulta (para conntrolar que no se llame a otra ficha si se atiende otra ficha)
RECHAZADO => Cuando el usuario no asiste a consulta y consultorio pasa a siguiente ticket
ATENDIDO => Cuando el usuario recibe la atencion en consultorio */
