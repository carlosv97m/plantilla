document.getElementById("form_paciente").addEventListener("submit", (e) => {
    e.preventDefault();
    if (e.target.checkValidity() === false) {
        e.stopPropagation();
    } else {
        enviarFormularioPaciente(e.target);
    }
    e.target.classList.add('was-validated');
})
document.getElementById('form_monturas_delete').addEventListener('submit', (e) => {
    e.preventDefault();
    borrar_monturas(e.target);
})
function cancelarRegistroPaciente() {
    document.querySelector('input[name="primerApellido"]').value = '';
    document.querySelector('input[name="segundoApellido"]').value = '';
    document.querySelector('input[name="nombre"]').value = '';
    // document.querySelector('input[name="apellidoEsposo"]').value = '';
    document.querySelector('input[name="ci"]').value = '';
    document.querySelector('select[name="genero_paciente"]').value = '';
    document.querySelector('input[name="fechaNacimiento"]').value = '';
    // document.querySelector('input[name="usuarioRegistro"]').value = '';
    document.querySelector('input[name="numeroHistoria"]').value = '';
    document.querySelector('input[name="ocupacion"]').value = '';
    document.querySelector('input[name="direccion"]').value = '';
    document.querySelector('select[name="idProcedencia"]').value = '';
    document.querySelector('input[name="telefono"]').value = '';
    document.querySelector('input[name="celular"]').value = '';
    document.querySelector('input[name="email"]').value = '';
    document.querySelector('select[name="idInstitucion"]').value = '';
    // document.querySelector('textarea[name="observacion"]').value = '';
    // document.querySelector('input[name="planTrabajo"]').value = '';
    // document.querySelector('textarea[name="motivo"]').value = '';
    getDataAboutPeople()
}
function enviarFormularioPaciente(e) {
    let tr_guardar = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    let primerApellido = tr_guardar.querySelector('input[name="primerApellido"]').value;
    let segundoApellido = tr_guardar.querySelector('input[name="segundoApellido"]').value;
    let nombre = tr_guardar.querySelector('input[name="nombre"]').value;
    let apellidoEsposo = tr_guardar.querySelector('input[name="apellidoEsposo"]').value;
    let idEstadoCivil = tr_guardar.querySelector('select[name="idEstadoCivil"]').value;
    let ci = tr_guardar.querySelector('input[name="ci"]').value;
    let sexo = tr_guardar.querySelector('select[name="genero_paciente"]').value;
    let fechaNacimiento = tr_guardar.querySelector('input[name="fechaNacimiento"]').value;

    let usuarioRegistro = tr_guardar.querySelector('input[name="usuarioRegistro"]').value;
    let numeroHistoria = tr_guardar.querySelector('input[name="numeroHistoria"]').value;
    let ocupacion = tr_guardar.querySelector('input[name="ocupacion"]').value;
    let direccion = tr_guardar.querySelector('input[name="direccion"]').value;
    let idProcedencia = tr_guardar.querySelector('select[name="idProcedencia"]').value;
    let telefono = tr_guardar.querySelector('input[name="telefono"]').value;
    let celular = tr_guardar.querySelector('input[name="celular"]').value;
    let email = tr_guardar.querySelector('input[name="email"]').value;
    let idInstitucion = tr_guardar.querySelector('select[name="idInstitucion"]').value;
    let observacion = tr_guardar.querySelector('textarea[name="observacion"]').value;
    let fechaRegistro = tr_guardar.querySelector('input[name="fechaRegistro"]').value;
    let fechaPrimeraConsulta = tr_guardar.querySelector('input[name="fechaPrimeraConsulta"]').value;
    let planTrabajo = tr_guardar.querySelector('input[name="planTrabajo"]').value;
    let motivo = tr_guardar.querySelector('textarea[name="motivo"]').value;

    // Validar que todos los campos estén llenos
    if (primerApellido && segundoApellido && nombre && apellidoEsposo && ci && sexo && idEstadoCivil && fechaNacimiento && usuarioRegistro && numeroHistoria && ocupacion && direccion && idProcedencia && telefono && celular && email && idInstitucion && planTrabajo && observacion && planTrabajo && motivo) {
        // alert(primerApellido + " - " + segundoApellido + " - " + nombre + " - " + apellidoEsposo + " - " + ci + " - " + sexo + " - " + fechaNacimiento + " - " + usuarioRegistro + " - " + numeroHistoria + " - " + ocupacion + " - " + direccion + " - " + idProcedencia + " - " + telefono + " - " + celular + " - " + email + " - " + idInstitucion + " - " + planTrabajo + " - " + observacion + " - " + planTrabajo + " - " + motivo + " + " + fechaPrimeraConsulta + " - " + fechaRegistro);
        // Cerrar el modal

        let url = "../ajax/paciente.php?op=guardar"
        let params = {
            primerApellido: primerApellido,
            segundoApellido: segundoApellido,
            nombre: nombre,
            apellidoEsposo: apellidoEsposo,
            ci: ci,
            sexo: sexo,
            idEstadoCivil: idEstadoCivil,
            fechaNacimiento: fechaNacimiento,
            usuarioRegistro: usuarioRegistro,
            numeroHistoria: numeroHistoria,
            ocupacion: ocupacion,
            direccion: direccion,
            idProcedencia: idProcedencia,
            telefono: telefono,
            celular: celular,
            email: email,
            observacion: observacion,
            idInstitucion: idInstitucion,
            planTrabajo: planTrabajo,
            motivo: motivo,
            fechaRegistro: fechaRegistro,
            fechaPrimeraConsulta: fechaPrimeraConsulta
            // idMontura: idMontura
        }
        axios.post(url, params)
            .then(res => {
                let dato = res;
                $('#registerPacient').modal('hide');
                console.log(dato);
                listar_pacientes();
            })
            .catch(err => {
                console.error(err);
            });
    } else {
        alert("Todos los campos deben estar llenos");
    }
}
function postQuote(id) {
    var button = document.querySelector(`button[data-id='${id}']`);
    var sex = button.getAttribute('data-sex');
    console.log(sex + " - " + id);
    // data-target="#ModalPostQuote"
    getGafas(sex);

    getMontura();

    $("#ModalPostQuote").modal('show');

}
async function getGafas(genero) {
    let url = '../ajax/paciente.php?op=getGafas';
    let data = { genero: genero }
    await axios.post(url, data)
        .then(res => {
            // console.log("gafas::", res.data);
            let dato = res;
            const select = document.getElementById('idGafas');
            res.data.forEach(gafas => {
                const option = document.createElement('option');
                option.value = gafas.idGafas;
                option.text = gafas.marca + " / " + gafas.modelo + " / " + gafas.color + " / " + gafas.precioUno + " / " + gafas.precioDos;
                select.add(option);
            });
        })
        .catch(err => {
            console.error(err);
        });
}
async function getMontura() {
    let url = '../ajax/paciente.php?op=getMontura';
    await axios.get(url)
        .then(res => {
            // console.log("Monturas::: ",res.data);
            let dato = res;
            const select = document.getElementById('idMontura');
            res.data.forEach(montura => {
                const option = document.createElement('option');
                option.value = montura.idMontura;
                option.text = montura.marca + " / " + montura.modelo + " / " + montura.color + " / " + montura.precioUno + " / " + montura.precioDos;
                select.add(option);
            });
        })
        .catch(err => {
            console.error(err);
        });
}
function listar_pacientes() {
    axios.get('../ajax/paciente.php?op=listar')
        .then(res => {
            // console.log("Datos recibidos:", res.data);
            const contenedor = document.getElementById('contenedor_pacientes');
            let cadena = "";
            document.getElementById('totalPagesPaciente').value = res.data.pagination;
            document.getElementById('currentPagePaciente').value = res.data.lenght[0].numero_de_registros;
            var desdePaciente = document.getElementById('initPaciente').value;
            // document.getElementById('inicio').innerHTML = desdePaciente;
            document.getElementById('desdePaciente').innerHTML = (desdePaciente * 10);
            document.getElementById('hastaPaciente').innerHTML = res.data.lenght[0].numero_de_registros;
            var totalPagesPaciente = document.getElementById('totalPagesPaciente').value;
            // console.log("totalPagesPaciente: " + totalPagesPaciente);
            var currentPagePaciente = document.getElementById('currentPagePaciente').value;
            // console.log("currentPagePaciente: " + currentPagePaciente);
            if (res.data.info.length > 0) {
                res.data.info.forEach((paciente) => {
                    // console.log("paciente actual:", paciente);
                    let validationDate = paciente.fechaRegistro;
                    let fechaFormateada = ''
                    if (validationDate) {
                        let fechaRegistro = new Date(paciente.fechaRegistro.date);
                        if (fechaRegistro) {
                            fechaFormateada = fechaRegistro.toLocaleDateString('es-ES') + ' ' + fechaRegistro.toLocaleTimeString('es-ES');
                        }
                    }
                    cadena += `<tr class="tr-shadow">
                    <td>${paciente.nombre} ${paciente.primerApellido} ${paciente.segundoApellido}</td>
                    <td>${paciente.ci}</td>
                    <td>${paciente.numeroHistoria}</td>
                    <td>${paciente.ocupacion}</td>
                    <td>${paciente.direccion}</td>
                    <td>${paciente.telefono}</td>
                    <td>${paciente.motivo}</td>
                    <td>${fechaFormateada}</td>
                    <td>
                        <div class="table-data-feature d-flex justify-content-around">
                            <button type="button" class="btn btn-primary btn-circle btn-sm edit_paciente" data-toggle="modal"  data-id="${paciente.idConsulta}" data-sex="${paciente.sexo}" onclick="postQuote(${paciente.idConsulta})">
                                <i class="zmdi zmdi-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-circle btn-sm" data-toggle="tooltip" data-id="${paciente.idConsulta}" onclick="deletepaciente(${paciente.idConsulta})" title="Eliminar">
                                <i class="zmdi zmdi-delete btnBorrar"></i>
                            </button>
                        </div>
                    </td>

                </tr>
                <tr class="spacer"></tr>`;
                });
            } else {
                console.log("No se encontraron pacientes.");
                cadena = `<tr class="tr-shadow">
                        <td colspan="8" align="center">No se registraron pacientes.</td>    
                     </tr>
                     <tr class="spacer"></tr>`;
            }
            contenedor.innerHTML = cadena;
        })
        .catch(err => {
            console.error(err);
        });

}
function prevPagePaciente() {
    var totalPagesPaciente = parseInt(document.getElementById('totalPagesPaciente').value);
    var start = parseInt(document.getElementById('initPaciente').value);

    // Solo disminuye 'start' si no es el inicio
    if (start > 1) {
        start--;
        document.getElementById('initPaciente').value = start;
        listIntervalPaciente(start);
    }
}
function nextPagePaciente() {
    var totalPagesPaciente = parseInt(document.getElementById('totalPagesPaciente').value);
    var start = parseInt(document.getElementById('initPaciente').value);

    // Solo aumenta 'start' si no es el final
    if (start < totalPagesPaciente) {
        start++;
        document.getElementById('initPaciente').value = start;
        listIntervalPaciente(start);
    }
}
function listIntervalPaciente(start) {
    // console.log("interval cambia a::", start);
    let data = { orden: start }
    axios.post('../ajax/paciente.php?op=listar', data)
        .then(res => {
            // console.log("Datos recibidos:", res.data);
            const contenedor = document.getElementById('contenedor_pacientes');
            let cadena = "";
            document.getElementById('totalPagesPaciente').value = res.data.pagination;
            document.getElementById('currentPagePaciente').value = res.data.lenght[0].numero_de_registros;
            var desdePaciente = document.getElementById('initPaciente').value;
            // document.getElementById('inicio').innerHTML = desdePaciente;
            document.getElementById('desdePaciente').innerHTML = (desdePaciente * 10);
            document.getElementById('hastaPaciente').innerHTML = res.data.lenght[0].numero_de_registros;
            var totalPagesPaciente = document.getElementById('totalPagesPaciente').value;
            // console.log("totalPagesPaciente: " + totalPagesPaciente);
            var currentPagePaciente = document.getElementById('currentPagePaciente').value;
            // console.log("currentPagePaciente: " + currentPagePaciente);
            if (res.data.info.length > 0) {
                res.data.info.forEach((montura) => {
                    console.log("Montura actual:", montura);
                    let validationDate = montura.fechaRegistro;
                    let fechaFormateada = ''
                    if (validationDate) {
                        let fechaRegistro = new Date(montura.fechaRegistro.date);
                        if (fechaRegistro) {
                            fechaFormateada = fechaRegistro.toLocaleDateString('es-ES') + ' ' + fechaRegistro.toLocaleTimeString('es-ES');
                        }
                    }
                    cadena += `<tr class="tr-shadow">
                    <td>${montura.nombre} ${montura.primerApellido} ${montura.segundoApellido}</td>
                    <td>${montura.ci}</td>
                    <td>${montura.numeroHistoria}</td>
                    <td>${montura.ocupacion}</td>
                    <td>${montura.direccion}</td>
                    <td>${montura.telefono}</td>
                    <td>${montura.motivo}</td>
                    <td>${fechaFormateada}</td>
                    <td>
                        <div class="table-data-feature d-flex justify-content-around">
                            <button type="button" class="btn btn-primary btn-circle btn-sm edit_montura" data-toggle="modal" data-target="#myModalEditMontura" data-id="${montura.idConsulta}" onclick="editarMontura(${montura.idConsulta})">
                                <i class="zmdi zmdi-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-circle btn-sm" data-toggle="tooltip" data-id="${montura.idConsulta}" onclick="deleteMontura(${montura.idConsulta})" title="Eliminar">
                                <i class="zmdi zmdi-delete btnBorrar"></i>
                            </button>
                        </div>
                    </td>

                </tr>
                <tr class="spacer"></tr>`;
                });
            } else {
                console.log("No se encontraron monturas.");
                cadena = `<tr class="tr-shadow">
                        <td colspan="8" align="center">No se registraron monturas.</td>    
                     </tr>
                     <tr class="spacer"></tr>`;
            }
            contenedor.innerHTML = cadena;
        })
        .catch(err => {
            console.error(err);
        });
}
async function getInstitucion() {
    await axios.get('../ajax/paciente.php?op=getInstitucion')
        .then(res => {
            // console.log("Institucion", res.data);
            const select = document.getElementById('idInstitucion');
            res.data.forEach(institucion => {
                const option = document.createElement('option');
                option.value = institucion.idInstitucion; // Asume que cada institución tiene un campo 'id'
                option.text = institucion.institucion; // Asume que cada institución tiene un campo 'nombre'
                select.add(option);
            });
        })
        .catch(err => {
            console.error(err);
        });
}
async function getProcedencia() {
    await axios.get('../ajax/paciente.php?op=getProcedencia')
        .then(res => {
            // console.log("Procedencia", res.data);
            const select = document.getElementById('idProcedencia');
            res.data.forEach(procedencia => {
                const option = document.createElement('option');
                option.value = procedencia.idProcedencia;
                option.text = procedencia.procedencia;
                select.add(option);
            });
        })
        .catch(err => {
            console.error(err);
        });
}
async function getEstadoCivil() {
    await axios.get('../ajax/paciente.php?op=getEstadoCivil')
        .then(res => {
            // console.log("Procedencia", res.data);
            const select = document.getElementById('idEstadoCivil');
            res.data.forEach(procedencia => {
                const option = document.createElement('option');
                option.value = procedencia.idEstadoCivil;
                option.text = procedencia.estadoCivil;
                select.add(option);
            });
        })
        .catch(err => {
            console.error(err);
        });
}
function getDataAboutPeople() {
    getInstitucion()
    getProcedencia()
    getEstadoCivil()
}
function getCategoriaTipoCristales() {
    let url = '../ajax/paciente.php?op=getCategoriaTipoCristales';
    axios.get(url)
        .then(res => {
            // console.log("Procedencia", res.data);
            const select = document.getElementById('idCTCristales');
            res.data.forEach(procedencia => {
                const option = document.createElement('option');
                option.value = procedencia.idCategoriaTipoCristales;
                option.text = procedencia.nombreCategoria;
                select.add(option);
            });
        })
        .catch(err => {
            console.error(err);
        });
}
$(document).ready(function () {
    $("#idCTCristales").change(async function () {
        const idCTCristales = $(this).val();
        const url = "../ajax/paciente.php?op=getCategoriaCristales";
        console.log("test::", idCTCristales);
        const data = { idCTCristales: idCTCristales };
        await axios.post(url, data)
            .then(res => {
                // Vaciamos el segundo select
                $("#categoriaCristal").empty();

                // Agregamos una opción por defecto
                $("#categoriaCristal").append("<option value='' disabled selected>__SELECCIONE__</option>");

                // Recorremos las categorías y las agregamos al segundo select
                res.data.forEach(categoria => {
                    $("#categoriaCristal").append(`<option value='${categoria.idCategoriaCristales}'>${categoria.nombreCategoria}</option>`);
                });
            })
            .catch(err => {
                console.error(err);
            });

    });
});
$(document).ready(function () {
    $("#categoriaCristal").change(async function () {
        const idCCristales = $(this).val();
        const url = "../ajax/paciente.php?op=getCristal";
        console.log("test2::", idCCristales);
        const data = { idCCristales: idCCristales };
        await axios.post(url, data)
            .then(res => {
                console.log("ojos::",res.data);
                // Ojo Derecho
                $("#idCristalOD").empty();
                $("#idCristalOD").append("<option value='' disabled selected>__SELECCIONE__</option>");
                res.data.forEach(categoria => {
                    $("#idCristalOD").append(`<option value='${categoria.idCristales}' style="background-color: ${categoria.colorCategoria};">${categoria.nombreCategoria} - ${categoria.nombreCristal} P1-${categoria.precioUno} Bs.  P2-${categoria.precioDos} Bs.</option>`);
                });
                // Ojo Izquierdo
                $("#idCristalOI").empty();
                $("#idCristalOI").append("<option value='' disabled selected>__SELECCIONE__</option>");
                res.data.forEach(categoria => {
                    $("#idCristalOI").append(`<option value='${categoria.idCristales}' style="background-color: ${categoria.colorCategoria};">${categoria.nombreCategoria} - ${categoria.nombreCristal} P1-${categoria.precioUno} Bs.  P2-${categoria.precioDos} Bs.</option>`);
                });
            })
            .catch(err => {
                console.error(err);
            });

    });
});

listar_pacientes()
getDataAboutPeople()
getCategoriaTipoCristales()