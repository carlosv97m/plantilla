var lista_videos = [];
var player;

let llamado;
function mostrar() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = yyyy + '-' + mm + '-' + dd;
    cadena = "";
    contenido = document.getElementById('contenido');
    axios.get("../ajax/atencion.php?op=solicitudes&fecha=" + today)
        .then(res => {
            console.log(res.data);
            res.data.forEach((key) => {
                // estado = estado + document.getElementById('llamado').value;
                timbre=key[5];
                console.log("Veces llamado= "+timbre);
                if (key[3] == 'pendiente') {
                    cadena += `<tr class="ticket">
                    <td class="numero-ticket">${key[0]}</td>
                    <td>
                        Consultorio ${key[4]}
                        <a class="llamado" id="llamado">${key[5]}</a>
                    </td>`
                }
                if (key[3] == 'llamando' && key[5] > 0) {
                    cadena += `<tr class="ticket ticket-seleccionado">
                    <td class="numero-ticket">${key[0]}</td>
                    <td>
                        Consultorio ${key[4]}
                        
                        <i class="fas fa-circle text-success"></i>
                        <a class="llamado" id="llamado">${key[5]}</a>
                    </td>`
                    cadena += audio();
                }
                if (key[3] == 'atendiendo') {
                    cadena += `<tr class="ticket ticket-seleccionado">
                    <td class="numero-ticket">${key[0]}</td>
                    <td>
                        Consultorio ${key[4]}
                        <i class="fas fa-circle text-danger"></i>
                    </td>`
                }
                cadena += `</tr>`
            });
            contenido.innerHTML = cadena;
        })
        .catch(err => {
            console.error(err);
        })
}
function audio() {
    return '<audio src="../public/sounds/TIMBRE.mp3" autoplay></audio>';
}
setInterval(() => {
    mostrar();
}, 3000)

function listar_mensaje() {
    const url = "../ajax/mensaje.php?op=listar_mensaje";
    axios.get(url).then(res => {
        const dato = res.data.mensaje;
        console.log(dato,'error');
        document.querySelector('#mensajeMonitor').innerHTML = dato;
    }).catch(err => {
        console.error(err);
    })
}

function onYouTubeIframeAPIReady() {

    const url = "../ajax/video.php?op=obtener_links";
    axios.get(url)
        .then(res => {
            res.data.forEach((data) => {
                lista_videos.push(data.link.split('/')[3]);
            })
            player = new YT.Player('player', {
                height: '360',
                width: '640',
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange

                }
            });
        })
        .catch(err => {
            console.error(err);
        })
}
document.addEventListener('DOMContentLoaded', () => {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    listar_mensaje();
})

function onPlayerReady(event) {
    player.cuePlaylist(lista_videos)
    event.target.playVideo();
}



function onPlayerStateChange(event) {
    var done = false;
    if (event.data == YT.PlayerState.PLAYING && !done) {
        //setTimeout(stopVideo, 15000);
        done = true;
    }
}

function stopVideo() {
    player.stopVideo();
}