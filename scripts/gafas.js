document.getElementById("form_gafas").addEventListener("submit", (e) => {
    e.preventDefault();
    if (e.target.checkValidity() === false) {
        e.stopPropagation();
    } else {
        enviarFormularioGafas(e.target);
    }
    e.target.classList.add('was-validated');
})
document.getElementById("form_file_gafas").addEventListener("submit", (e) => {
    e.preventDefault();
    if (e.target.checkValidity() === false) {
        e.stopPropagation();
    } else {
        enviarFormularioArchivoGafas(e.target);
    }
    e.target.classList.add('was-validated');
})
document.getElementById("form_edit_gafas").addEventListener("submit", (e) => {
    e.preventDefault();
    if (e.target.checkValidity() === false) {
        e.stopPropagation();
    } else {
        modificar_gafas(e.target);
    }
    e.target.classList.add('was-validated');
})
document.getElementById("form_gafas_delete").addEventListener("submit", (e) => {
    e.preventDefault();
    borrar_gafas(e.target);
})
function enviarFormularioArchivoGafas(e) {
    const fileInput = document.getElementById('excelFileGafas');
    console.log(fileInput);
    if (!fileInput.files.length) {
        alert("Por favor selecciona un archivo para subir.");
        return;
    }

    const file = fileInput.files[0];
    const validExtensions = ['xls', 'xlsx'];
    const fileName = file.name;
    const fileExtension = fileName.split('.').pop().toLowerCase();
    if (!validExtensions.includes(fileExtension)) {
        alert("Por favor selecciona un archivo Excel (.xls or .xlsx).");
        return;
    }

    // Crear un objeto FormData
    let formData = new FormData();
    formData.append('excelFileGafas', file);
    console.table(formData);
    // Configurar la solicitud de axios
    let config = {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    };
    console.log("config:: ", config);
    // Enviar la solicitud a través de axios
    axios.post('../ajax/gafas.php?op=guardarFile', formData, config)
        .then(res => {
            $('#UpFileGafas').modal('hide');
            const data = res.data;
            console.log(data);
            listar_gafas();
        })
        .catch(err => {
            console.error(err);
        });
}
document.getElementById('contenedor_gafas').addEventListener('click', (e) => {
    if (e.target.classList.contains('btnEditar')) editar_gafas(e.target);
    // else if (e.target.classList.contains('btnBorrar')) borrar_gafas(e.target);
    else if (e.target.classList.contains('btnGuardar')) modificar_gafas(e.target);
    else if (e.target.type == "checkbox") accion_gafas(e.target)
})
function enviarFormularioGafas(e) {
    let tr_guardar = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    let marca = tr_guardar.querySelector('input[name="nombre_marca"]').value;
    let modelo = tr_guardar.querySelector('input[name="nombre_modelo"]').value;
    let color = tr_guardar.querySelector('input[name="nombre_color"]').value;
    let articulo = tr_guardar.querySelector('input[name="nombre_articulo"]').value;
    let cantidad = tr_guardar.querySelector('input[name="nombre_cantidad"]').value;
    let sexo = tr_guardar.querySelector('select[name="nombre_sexo"]').value;
    let precioUno = tr_guardar.querySelector('input[name="nombre_precioUno"]').value;
    let precioDos = tr_guardar.querySelector('input[name="nombre_precioDos"]').value;
    let idGafas = null;

    // Validar que todos los campos estén llenos
    if (marca && modelo && color && articulo && cantidad && sexo && precioUno && precioDos) {
        // alert(marca + modelo + color + articulo + cantidad);
        // Cerrar el modal
        $('#myModalGafas').modal('hide');
        let url = "../ajax/gafas.php?op=guardaryeditar"
        let params = {
            marca: marca,
            modelo: modelo,
            color: color,
            articulo: articulo,
            cantidad: cantidad,
            sexo: sexo,
            precioUno: precioUno,
            precioDos: precioDos,
            idGafas: idGafas
        }
        axios.post(url, params)
            .then(res => {
                let dato = res;
                console.log(dato);
                // location.reload();
                listar_gafas();
            })
            .catch(err => {
                console.error(err);
            });
    } else {
        alert("Todos los campos deben estar llenos");
    }

}
function accion_gafas(e) {
    const id_gafas = e.parentElement.parentElement.parentElement.querySelector('input[name="id_gafas"]').value;
    const opcion = e.checked ? 1 : 0;
    const url = "../ajax/gafas.php?op=activarydesactivar";
    const params = {
        id_video: id_gafas,
        opcion: opcion
    }
    axios.post(url, params)
        .then(res => {
            if (res.data) {
                opcion ? alert("Video Habilitado") : alert("Video Deshabiltado");
            }
        })
        .catch(err => {
            console.error(err);
        })
}
function editar_gafas(e) {
    let tr = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    let btnGuardar = tr.querySelector('.zmdi-edit');
    btnGuardar.classList.remove('zmdi-edit');
    btnGuardar.classList.remove('btnEditar');
    btnGuardar.parentElement.classList.remove('btnEditar');
    btnGuardar.classList.add('zmdi-save');
    btnGuardar.classList.add('btnGuardar');
    btnGuardar.parentElement.classList.add('btnGuardar');
    btnGuardar.parentElement.title = 'Guardar';
    mostrar_form_editar(tr);
}
async function borrar_gafas(e) {
    let tr = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    let id = tr.querySelector('input[name="idGafasDelete"]').value
    let url = "../ajax/gafas.php?op=eliminar"
    let params = { idGafas: id }
    await axios.post(url, params)
        .then(res => {
            if (res.data) {
                tr.remove();
                alert("Registro eliminado con exito!")
                listar_gafas()
                // console.log(res);
            }
        })
        .catch(err => {
            console.error(err);
        })
}
function mostrar_form_editar(tr) {
    // console.log(tr.getElementsByClassName('editar'))
    let tabla = tr.getElementsByClassName('tabla')
    tabla[0].style.display = 'none'
    tabla[1].style.display = 'none'
    tabla[2].style.display = 'none'
    tabla[3].style.display = 'none'
    tabla[4].style.display = 'none'
    tabla[7].style.display = 'none'
    tabla[8].style.display = 'none'
    let editar = tr.getElementsByClassName('editar')
    editar[0].style.display = 'block'
    editar[1].style.display = 'block'
    editar[2].style.display = 'block'
    editar[3].style.display = 'block'
    editar[4].style.display = 'block'
    editar[7].style.display = 'block'
    editar[8].style.display = 'block'
}
function editarGafas(idGafas) {
    console.log("ID de la montura:", idGafas);
    let info = getInfo(idGafas)
    if (info) {
        $('#myModalGafasEdit').modal('show');
        document.getElementById('idGafasEdit').value = '';
        document.getElementById('edit_gafas_marca').value = '';
        document.getElementById('edit_gafas_modelo').value = '';
        document.getElementById('edit_gafas_color').value = '';
        document.getElementById('edit_gafas_articulo').value = '';
        document.getElementById('edit_gafas_cantidad').value = '';
        document.getElementById('edit_gafas_precioUno').value = '';
        document.getElementById('edit_gafas_precioDos').value = '';
    }
}
async function getInfo(id) {
    // console.log("id Heredado!::", id);
    if (!id) {
        alert("Error interno, intente mas tarde!")
    }
    let url = '../ajax/gafas.php?op=obtener_data';
    let params = { idGafas: id };
    await axios.post(url, params)
        .then(res => {
            // console.log("Encontrados",[res.data]);
            document.getElementById('idGafasEdit').value = id;
            res.data.forEach((montura) => {
                document.getElementById('edit_gafas_marca').value = montura.marca;
                document.getElementById('edit_gafas_modelo').value = montura.modelo;
                document.getElementById('edit_gafas_color').value = montura.color;
                document.getElementById('edit_gafas_articulo').value = montura.articulo;
                document.getElementById('edit_gafas_cantidad').value = montura.cantidad;
                document.getElementById('edit_gafas_precioUno').value = montura.precioUno;
                document.getElementById('edit_gafas_precioDos').value = montura.precioDos;
            })
        })
        .catch(err => {
            console.error(err);
        })
    console.log(id);
}
function modificar_gafas(e) {
    let tr_guardar = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    let marca = tr_guardar.querySelector('input[name="edit_gafas_marca"]').value;
    let modelo = tr_guardar.querySelector('input[name="edit_gafas_modelo"]').value;
    let color = tr_guardar.querySelector('input[name="edit_gafas_color"]').value;
    let articulo = tr_guardar.querySelector('input[name="edit_gafas_articulo"]').value;
    let cantidad = tr_guardar.querySelector('input[name="edit_gafas_cantidad"]').value;
    let precioUno = tr_guardar.querySelector('input[name="edit_gafas_precioUno"]').value;
    let precioDos = tr_guardar.querySelector('input[name="edit_gafas_precioDos"]').value;
    let idGafas = tr_guardar.querySelector('input[name="idGafasEdit"]').value;
    if (marca != "" && modelo != "" && color != "" && articulo != "" && cantidad != "" && idGafas != "") {
        // alert(idGafas + marca + modelo + color + articulo + cantidad);
        let url = "../ajax/gafas.php?op=guardaryeditar"
        let params = {
            marca: marca,
            modelo: modelo,
            color: color,
            articulo: articulo,
            cantidad: cantidad,
            precioUno: precioUno,
            precioDos: precioDos,
            idGafas: idGafas
        }
        axios.post(url, params)
            .then(res => {
                let dato = res.data;
                console.log(res.data);
                listar_gafas();
                $('#myModalGafasEdit').modal('hide');
                alert("Registro modificado con exito!");
            })
            .catch(err => {
                console.error(err);
            })
    } else {
        alert("Llene los campos con el formato correcto");
    }
}
function deleteGafas(idGafas) {
    console.log("ID de la montura Delete:", idGafas);
    $('#myModalDeleteGafas').modal('show');
    document.getElementById('idGafasDelete').value = idGafas;
}
function deleteGafasClose() {
    $('#myModalDeleteGafas').modal('hide');
}
function listar_gafas() {
    axios.get('../ajax/gafas.php?op=listar')
        .then(res => {
            // console.log(res.data);
            const contenedor = document.getElementById('contenedor_gafas');
            let cadena = "";
            let sexo = "";
            document.getElementById('totalPagesGafas').value = res.data.pagination;
            document.getElementById('currentPageGafas').value = res.data.lenght[0].numero_de_registros;
            var desdeGafas = document.getElementById('initGafas').value;
            document.getElementById('desdeGafas').innerHTML = (desdeGafas * 10);
            document.getElementById('hastaGafas').innerHTML = res.data.lenght[0].numero_de_registros;
            var totalPagesGafas = document.getElementById('totalPagesGafas').value;
            // console.log("totalPagesGafas: " + totalPagesGafas);
            var currentPageGafas = document.getElementById('currentPageGafas').value;
            // console.log("currentPageGafas: " + currentPageGafas);
            if (res.data.info.length > 0) {
                // console.log(res.data);
                res.data.info.forEach((gafas) => {
                    let fechaRegistro = new Date(gafas.fechaRegistro.date);
                    let fechaFormateada = fechaRegistro.toLocaleDateString('es-ES') + ' ' + fechaRegistro.toLocaleTimeString('es-ES');
                    (gafas.genero === 'V') ? sexo = 'Varones' : sexo = 'Mujeres'
                    cadena += `<tr class="tr-shadow">
                    <td>${gafas.marca}</td>
                    <td>${gafas.modelo}</td>
                    <td>${gafas.color}</td>
                    <td>${gafas.articulo}</td>
                    <td>${gafas.cantidad}</td>
                    <td>${sexo}</td>
                    <td>${fechaFormateada}</td>
                    <td>${gafas.precioUno}</td>
                    <td>${gafas.precioDos}</td>
                    <td>
                        <div class="table-data-feature">
                            <button type="button" class="btn btn-primary btn-circle btn-sm edit_Gafas" data-toggle="modal" data-target="#myModalEditGafas" data-id="${gafas.idGafas}" onclick="editarGafas(${gafas.idGafas})">
                                <i class="zmdi zmdi-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-circle btn-sm" data-toggle="tooltip" data-id="${gafas.idGafas}" onclick="deleteGafas(${gafas.idGafas})" title="Eliminar">
                                <i class="zmdi zmdi-delete btnBorrar"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr class="spacer"></tr>`
                })

            } else {
                cadena = `<tr class="tr-shadow">
                        <td colspan="10" align="center">No se registraron gafas.</td>    
                        </tr>
                <tr class="spacer"></tr>`
            }
            contenedor.innerHTML = cadena;
        })
        .catch(err => {
            console.error(err);
        })
}
function prevPageGafas() {
    var totalPagesGafas = parseInt(document.getElementById('totalPagesGafas').value);
    var start = parseInt(document.getElementById('initGafas').value);

    // Solo disminuye 'start' si no es el inicio
    if (start > 1) {
        start--;
        document.getElementById('initGafas').value = start;
        listIntervalGafas(start);
    }
}
function nextPageGafas() {
    var totalPagesGafas = parseInt(document.getElementById('totalPagesGafas').value);
    var start = parseInt(document.getElementById('initGafas').value);

    // Solo aumenta 'start' si no es el final
    if (start < totalPagesGafas) {
        start++;
        document.getElementById('initGafas').value = start;
        listIntervalGafas(start);
    }
}
function listIntervalGafas(start) {
    // console.log("interval cambia a::", start);
    let data = { orden: start }
    axios.post('../ajax/gafas.php?op=listar', data)
        .then(res => {
            // console.log(res.data);
            const contenedor = document.getElementById('contenedor_gafas');
            let cadena = "";
            let sexo = "";
            document.getElementById('totalPagesGafas').value = res.data.pagination;
            document.getElementById('currentPageGafas').value = res.data.lenght[0].numero_de_registros;
            var desdeGafas = document.getElementById('initGafas').value;
            document.getElementById('desdeGafas').innerHTML = (desdeGafas * 10);
            document.getElementById('hastaGafas').innerHTML = res.data.lenght[0].numero_de_registros;
            var totalPagesGafas = document.getElementById('totalPagesGafas').value;
            // console.log("totalPagesGafas: " + totalPagesGafas);
            var currentPageGafas = document.getElementById('currentPageGafas').value;
            // console.log("currentPageGafas: " + currentPageGafas);
            if (res.data.info.length > 0) {
                // console.log(res.data);
                res.data.info.forEach((gafas) => {
                    let fechaRegistro = new Date(gafas.fechaRegistro.date);
                    let fechaFormateada = fechaRegistro.toLocaleDateString('es-ES') + ' ' + fechaRegistro.toLocaleTimeString('es-ES');
                    (gafas.genero === 'V') ? sexo = 'Varones' : sexo = 'Mujeres'
                    cadena += `<tr class="tr-shadow">
                    <td>${gafas.marca}</td>
                    <td>${gafas.modelo}</td>
                    <td>${gafas.color}</td>
                    <td>${gafas.articulo}</td>
                    <td>${gafas.cantidad}</td>
                    <td>${sexo}</td>
                    <td>${fechaFormateada}</td>
                    <td>${gafas.precioUno}</td>
                    <td>${gafas.precioDos}</td>
                    <td>
                        <div class="table-data-feature d-flex justify-content-around">
                            <button type="button" class="btn btn-primary btn-circle btn-sm edit_Gafas" data-toggle="modal" data-target="#myModalEditGafas" data-id="${gafas.idGafas}" onclick="editarGafas(${gafas.idGafas})">
                                <i class="zmdi zmdi-edit"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-circle btn-sm" data-toggle="tooltip" data-id="${gafas.idGafas}" onclick="deleteGafas(${gafas.idGafas})" title="Eliminar">
                                <i class="zmdi zmdi-delete btnBorrar"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr class="spacer"></tr>`
                })

            } else {
                cadena = `<tr class="tr-shadow">
                        <td colspan="10" align="center">No se registraron gafas.</td>    
                        </tr>
                <tr class="spacer"></tr>`
            }
            contenedor.innerHTML = cadena;
        })
        .catch(err => {
            console.error(err);
        })
}
listar_gafas()