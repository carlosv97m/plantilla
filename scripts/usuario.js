function insertar(nombre, user, password, sexo, telefono, foto, cargo, fecha_registro, fecha_actualizacion, fecha_ultimo_ingreso, activo) {
    const params = {
        nombre: nombre,
        user: user,
        password: password,
        sexo: sexo,
        telefono: telefono,
        foto: foto,
        cargo: cargo,
        fecha_registro: fecha_registro,
        fecha_actualizacion: fecha_actualizacion,
        fecha_ultimo_ingreso: fecha_ultimo_ingreso,
        activo: activo
    }
    const url = "../ajax/usuario.php?op=guardar"
    axios.post(url, params)
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.error(err);
        })
}
function generar_ticket(cola) {
    let inicial_cola = "";
    let contador_cola = 0;
    let fecha = new Date()
    let iframe = document.createElement("iframe");
    let contenedor = document.createElement("div");
    let p_num_ticket = document.createElement("p");
    let p_ticket_cola = document.createElement("p");
    let hora_ticket = document.createElement("p");
    let banco = document.createElement("p");

    contador++;
    if (cola == "Fidelizado") {
        contador_fidelizado++;
        inicial_cola = "DF-";
        contador_cola = contador_fidelizado;
    } else if (cola == "Distribucion") {
        contador_distribucion++;
        inicial_cola = "DS-";
        contador_cola = contador_distribucion;
    } else if (cola == "Voluntario") {
        contador_voluntario++;
        inicial_cola = "DV-";
        contador_cola = contador_voluntario;
    }


    iframe.style.display = "none";


    p_num_ticket.innerText = "N° " + contador;
    p_num_ticket.style.textAlign = "center";
    p_ticket_cola.style.fontSize = "20px";


    p_ticket_cola.innerText = inicial_cola + contador_cola;
    p_ticket_cola.style.textAlign = "center";
    p_ticket_cola.style.fontSize = "25px";

    hora_ticket.innerText = `${fecha.getDate()}/${fecha.getMonth() + 1}/${fecha.getFullYear()} ${fecha.getHours() < 10 ? "0" + fecha.getHours() : fecha.getHours()}:${fecha.getMinutes() < 10 ? "0" + fecha.getMinutes() : fecha.getMinutes()}`;
    hora_ticket.style.textAlign = "center";

    banco.innerText = "Banco de Sangre"
    banco.style.textAlign = "center";



    contenedor.appendChild(p_num_ticket);
    contenedor.appendChild(p_ticket_cola);
    contenedor.appendChild(hora_ticket);
    contenedor.appendChild(banco);

    iframe.srcdoc = contenedor.innerHTML;
    document.body.appendChild(iframe);
    iframe.focus();
    iframe.contentWindow.print();

    codigo_cola = inicial_cola + contador_cola;
    fecha_ticket = `${fecha.getFullYear()}-${fecha.getMonth() + 1}-${fecha.getDate()}`;
    hora_ticket = `${fecha.getHours() < 10 ? "0" + fecha.getHours() : fecha.getHours()}:${fecha.getMinutes() < 10 ? "0" + fecha.getMinutes() : fecha.getMinutes()}`;
    id_cola = inicial_cola == "DF-" ? 1 : inicial_cola == "DV-" ? 2 : 3;
    guardar_ticket(codigo_cola, fecha_ticket, hora_ticket, id_cola, contador, contador_fidelizado, contador_voluntario, contador_distribucion);

}
function consultar_ticket() {
    let hoy = new Date();
    let fecha = `${hoy.getFullYear()}-${hoy.getMonth() + 1}-${hoy.getDate()}`;
    axios.get('../ajax/ticket.php?op=verificar_ticket&fecha=' + fecha)
        .then(res => {
            console.log(res.data);
            if (res.data) {
                contador = localStorage.getItem('contador');
                contador_fidelizado = localStorage.getItem('fidelizado');
                contador_voluntario = localStorage.getItem('voluntario');
                contador_distribucion = localStorage.getItem('distribucion');
            } else {
                localStorage.setItem('contador', 0);
                localStorage.setItem('fidelizado', 0);
                localStorage.setItem('voluntario', 0);
                localStorage.setItem('distribucion', 0);
                contador = localStorage.getItem('contador');
                contador_fidelizado = localStorage.getItem('fidelizado');
                contador_voluntario = localStorage.getItem('voluntario');
                contador_distribucion = localStorage.getItem('distribucion');
            }
        })
        .catch(err => {
            console.error(err);
        })
}

consultar_ticket();