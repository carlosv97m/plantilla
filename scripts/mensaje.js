document.getElementById("form_mensaje").addEventListener("submit", (e) => {
    e.preventDefault();
    if (e.target.checkValidity() === false) {
        e.stopPropagation();
    } else {
        enviarFormulario(e.target);
    }
    e.target.classList.add('was-validated');
})

function enviarFormulario(e) {
    let tr_guardar = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    const mensaje = tr_guardar.querySelector('textarea[name="mensajeMonitor"]').value;
    const id = document.getElementById("id_mensaje").value;
    if (id == 0) {
        const url = "../ajax/mensaje.php?op=guardaryeditar";
        const params = { id_mensaje: id, mensaje: mensaje };
        // console.log(params, url);
        axios.post(url, params).then(res => {
            
            console.log("save",res.data);
            alert("Guardado con Éxito");
            // form.querySelector('#mensajeMonitor').value = res;
        }).catch(err => {
            console.error(err);
            // console.error(err.response);
        })
    } else {
        const url = "../ajax/mensaje.php?op=guardaryeditar";
        const params = { id_mensaje: id, mensaje: mensaje };
        console.log("update" ,params, url);
        axios.post(url, params).then(res => {
            alert("Modificado con Éxito");
            // form.querySelector('#mensajeMonitor').value = dato;
        }).catch(err => {
            console.error(err);
            // console.error(err.response);
        })
    }
}

function listar_mensaje() {
    const url = "../ajax/mensaje.php?op=listar_mensaje";
    axios.get(url).then(res => {
        console.log("info:: ", res.data);
        let dato = "";
        if (res.data.id_memsaje != 0) {
            dato = res.data.mensaje;
            // console.log("1 ",dato);
            document.getElementById('mensajeMonitor').value = dato;
            document.getElementById('id_mensaje').value = res.data.id_mensaje;
        } else {
            dato = "Sin Mensajes aun!."
            // console.log("2", dato);
            document.getElementById('id_mensaje').value = 0;
            document.getElementById('mensajeMonitor').value = dato;
        }
    }).catch(err => {
        console.error(err.response);
    })
}

listar_mensaje();
