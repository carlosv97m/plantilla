document.getElementById("frmAcceso").addEventListener("submit", (e) => {
    e.preventDefault();
    login = document.getElementById("username").value;
    clave = document.getElementById("clave").value;
    const options = {
        login: login,
        clave: clave
    }
    axios.post('../ajax/usuario.php?op=verificar', options)
        .then((res) => {
            // console.log([res.data]);
            // alert(res.data)
            if (res.data !== null) {
                // console.log(res.data);
                window.location = "usuario.php"
            } else {
                alert("Credenciales Incorrectos");
            }
        })
        .catch(function (err) {
            console.log(err)
        })
})