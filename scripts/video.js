document.getElementById("form_video").addEventListener("submit", (e) => {
    e.preventDefault();
    if (e.target.checkValidity() === false) {
        e.stopPropagation();
    } else {
        enviarFormularioVideo(e.target);
    }
    e.target.classList.add('was-validated');
})
document.getElementById('contenedor_videos').addEventListener('click', (e) => {
    if (e.target.classList.contains('btnEditar')) editar_video(e.target);
    else if (e.target.classList.contains('btnBorrar')) borrar_video(e.target);
    else if (e.target.classList.contains('btnGuardar')) guardar_video(e.target);
    else if (e.target.type == "checkbox") accion_video(e.target)
})

function accion_video(e) {
    const id_video = e.parentElement.parentElement.parentElement.querySelector('input[name="id_video"]').value;
    const opcion = e.checked ? 1 : 0;
    const url = "../ajax/video.php?op=activarydesactivar";
    const params = {
        id_video: id_video,
        opcion: opcion
    }
    axios.post(url, params)
        .then(res => {
            if (res.data) {
                opcion ? alert("Video Habilitado") : alert("Video Deshabiltado");
            }
        })
        .catch(err => {
            console.error(err);
        })
}

function editar_video(e) {
    let tr = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    let btnGuardar = tr.querySelector('.zmdi-edit');
    btnGuardar.classList.remove('zmdi-edit');
    btnGuardar.classList.remove('btnEditar');
    btnGuardar.parentElement.classList.remove('btnEditar');
    btnGuardar.classList.add('zmdi-save');
    btnGuardar.classList.add('btnGuardar');
    btnGuardar.parentElement.classList.add('btnGuardar');
    btnGuardar.parentElement.title = 'Guardar';
    mostrar_form_editar(tr);
}

function borrar_video(e) {
    let tr = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    let id_video = tr.querySelector('input[name="id_video"]').value
    let url = "../ajax/video.php?op=eliminar"
    let params = { id_video: id_video }
    axios.post(url, params)
        .then(res => {
            if (res.data) {
                tr.remove();
                alert("Video Eliminado")
            }
        })
        .catch(err => {
            console.error(err);
        })
}

function guardar_video(e) {
    let tr_guardar = e.classList.contains('item') ? e.parentElement.parentElement.parentElement : e.parentElement.parentElement.parentElement.parentElement;
    let nombre = tr_guardar.querySelector('input[name="nombre_video"]').value;
    let link = tr_guardar.querySelector('input[name="link_video"]').value;
    let id_video = tr_guardar.querySelector('input[name="id_video"]').value;
    if (nombre != "" && link != "" && validarLink(link)) {
        let url = "../ajax/video.php?op=guardaryeditar"
        let params = { nombre: nombre, link: link, id_video: id_video }
        axios.post(url, params)
            .then(res => {
                let dato = res.data;
                let cadena = `
            <td>
                <label class="au-checkbox">
                    <input type="checkbox" checked=${dato.habilitado == 1 ? "checked" : ""}>
                    <span class="au-checkmark"></span>
                </label>
            </td>
            <form class="needs-validation" novalidate>
            <td>
                <input class="editar form-control" style="display:none;" name="nombre_video" type="text" value="${dato.nombre}" required>
                <divsclass="tabla">${dato.nombre}</div>
            </td>
            <td>
                <input class="editar form-control" style="display:none;" name="link_video" type="url" value="${dato.link}" required>
                <span class="block-email tabla">
                    <a href="${dato.link}" target="_blank">
                        ${dato.link}
                    </a>
                </span>
            </td>
            <input type="hidden" value="${dato.id_video}" name="id_video">
            </form>
            <td>
                <div class="table-data-feature">
                    <button class="item" data-toggle="tooltip" data-placement="top" title="Editar">
                        <i class="zmdi zmdi-edit"></i>
                    </button>
                    <button class="item" data-toggle="tooltip" data-placement="top" title="Eliminar">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>
            </td>`;
                tr_guardar.innerHTML = cadena;
            })
            .catch(err => {
                console.error(err);
            })
    } else {
        alert("Llene los campos con el formato correcto");
    }
}

function mostrar_form_editar(tr) {
    // console.log(tr.getElementsByClassName('editar'))
    let tabla = tr.getElementsByClassName('tabla')
    tabla[0].style.display = 'none'
    tabla[1].style.display = 'none'
    let editar = tr.getElementsByClassName('editar')
    editar[0].style.display = 'block'
    editar[1].style.display = 'block'
}

function validarLink(url) {
    let flag;
    const video = url.substr(0, 16);
    if (video == 'https://youtu.be') {
        flag = true;
    } else {
        flag = false;
    }
    return flag;
}


function enviarFormularioVideo(form) {
    const existe_id = form.querySelector('input[name="id_video"]');
    const nombre = form.querySelector('input[name="nombre_video"]').value;
    const link = form.querySelector('input[name="link_video"]').value;
    if (validarLink(link)) {
        const url = "../ajax/video.php?op=guardaryeditar";
        const params = { nombre: nombre, link: link };
        const contenedor = document.getElementById('contenedor_videos');
        axios.post(url, params)
            .then(res => {
                const dato = res.data;
                let cadena = `
                    <td>
                        <label class="au-checkbox">
                            <input type="checkbox" checked=${dato.habilitado == 1 ? "checked" : ""}>
                            <span class="au-checkmark"></span>
                        </label>
                    </td>
                    <form class="needs-validation" novalidate>
                    <td>
                    <input class="editar form-control" style="display:none;" name="nombre_video" type="text" value="${dato.nombre}" required>
                    <div class="tabla">${dato.nombre}</div>
                    </td>
                    <td>
                    <input class="editar form-control" style="display:none;" name="link_video" type="url" value="${dato.link}" required>
                        <span class="block-email tabla">
                            <a href="${dato.link}" target="_blank">
                                ${dato.link}
                            </a>
                        </span>
                    </td>
                    <input type="hidden" value="${dato.id_video}" name="id_video">
                    </form>
                    <td>
                        <div class="table-data-feature">
                            <button class="item" data-toggle="tooltip" data-placement="top" title="Editar">
                                <i class="zmdi zmdi-edit"></i>
                            </button>
                            <button class="item" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                <i class="zmdi zmdi-delete"></i>
                            </button>
                        </div>
                    </td>`;
                if (contenedor.querySelector('td').innerText == "No se registraron videos.") {
                    contenedor.innerHTML = "";
                }
                document.getElementById("nombre_video").value = "";
                document.getElementById("link_video").value = "";
                form.classList.remove('was-validated');


                const tr = document.createElement("tr");
                const spacer = document.createElement("tr")
                tr.classList.add('tr-shadow')
                tr.innerHTML = cadena;
                spacer.classList.add('spacer')
                contenedor.appendChild(spacer)
                contenedor.appendChild(tr)
                contenedor.appendChild(spacer)

            })
            .catch(err => {
                console.error(err);
            })

    } else {
        alert("Error...Introducir un video de Youtube")
    }

}

function listar_videos() {
    // alert("Listar");
    axios.get('../ajax/video.php?op=listar')
        .then(res => {
            const contenedor = document.getElementById('contenedor_videos');
            let cadena = "";
            // console.log("Video");
            // console.log(res.data);
            if (res.data.info.length > 0) {
                res.data.info.forEach((video) => {
                    // console.log("Pruebas::", video);
                    cadena += `<tr class="tr-shadow">
                    <td>
                        <label class="au-checkbox">
                            <input type="checkbox" ${video.habilitado == 1 ? "checked" : ""}>
                            <span class="au-checkmark"></span>
                        </label>
                    </td>
                    <form class="needs-validation" novalidate>
                    <td>
                    <input class="editar form-control" style="display:none;" name="nombre_video" type="text" value="${video.nombre}" required>
                    <div class="tabla"> ${video.nombre} </div>
                    </td>
                    <td>
                        <input class="editar form-control" style="display:none;" name="link_video" type="url" value="${video.link}" required>
                        <span class="block-email tabla">
                            <a href="${video.link}" target="_blank">
                                ${video.link}
                            </a>
                        </span>
                    </td>
                    <input type="hidden" value="${video.id_video}" name="id_video">
                    </form>
                    <td>
                        <div class="table-data-feature">
                            <button class="item btnEditar" data-toggle="tooltip" data-placement="top" title="Editar">
                                <i class="zmdi zmdi-edit btnEditar"></i>
                            </button>
                            <button class="item btnBorrar" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                <i class="zmdi zmdi-delete btnBorrar"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr class="spacer"></tr>`
                })

            } else {
                cadena = `<tr class="tr-shadow">
                        <td colspan="4" align="center">No se registraron videos.</td>    
                        </tr>
                <tr class="spacer"></tr>`
            }
            contenedor.innerHTML = cadena;
        })
        .catch(err => {
            console.error(err);
        })
}
listar_videos()