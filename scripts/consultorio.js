let estado = "pendiente"; // No existen tickets en cola o no se inicio atencion
function obtener_pendientes() {
    const id_cola = document.getElementById('tipo_usuario').value;
    const url = "../ajax/consultorio.php?op=listar_pendientes";
    const params = {
        id_cola: id_cola
    }
    axios.post(url, params)
        .then(res => {
            dibujar_tickets(res.data.num_cola, res.data.atender);
        })
        .catch(err => {
            console.error(err);
        })
}

function cambiar_estado(nuevo_estado, veces_llamado = 0) {
    const id_ticket = document.getElementById("atendiendo_id").value;
    const url = "../ajax/consultorio.php?op=cambiar_estado";
    estado = nuevo_estado
    const params = {
        estado: estado,
        id_ticket: id_ticket,
        veces_llamado: veces_llamado
    }
    axios.post(url, params)
        .then(res => {
            if (estado == "atendido" || estado == "rechazado") {
                obtener_pendientes();
            } else {
                if (res.data) activar_opciones(estado)
            }

        })
        .catch(err => {
            console.error(err);
        })
}

// setInterval(() => {
//     obtener_pendientes();
// }, 5000)

function dibujar_tickets(num_cola, tickets) {
    document.getElementById("num_cola").innerText = num_cola
    if (tickets.length == 2) {
        let estado = tickets[0].estado_ticket;
        document.getElementById("atendiendo").innerHTML = tickets[0].codigo_ticket;
        document.getElementById("siguiente").innerHTML = tickets[1].codigo_ticket;
        document.getElementById("atendiendo_id").value = tickets[0].id_ticket;
        document.getElementById("veces_llamado").value = tickets[0].veces_llamado;

        activar_opciones(estado);
    } else if (tickets.length == 1) {
        let estado = tickets[0].estado_ticket;
        document.getElementById("atendiendo").innerHTML = tickets[0].codigo_ticket;
        document.getElementById("siguiente").innerHTML = "XX-XX"
        document.getElementById("atendiendo_id").value = tickets[0].id_ticket
        document.getElementById("veces_llamado").value = tickets[0].veces_llamado;

        activar_opciones(estado);
    } else {
        let estado = "pendiente";
        document.getElementById("atendiendo").innerHTML = "XX-XX";
        document.getElementById("siguiente").innerHTML = "XX-XX";
        document.getElementById("atendiendo_id").value = "";
        document.getElementById("veces_llamado").value = 0;
        activar_opciones(estado, false)
    }
}

function activar_opciones(estado, flag = true) {
    const btnIniciar = document.getElementById("btnIniciar")
    if (estado === "pendiente") {
        document.getElementById("iniciar_atencion").style.display = "block";
        document.getElementById("opciones").style.display = "none";
        document.getElementById("terminar_atencion").style.display = "none";

        flag ? btnIniciar.disabled = false : btnIniciar.disabled = true;
    } else if (estado === "llamando") {
        document.getElementById("iniciar_atencion").style.display = "none";
        document.getElementById("opciones").style.display = "block";
        document.getElementById("terminar_atencion").style.display = "none";

    } else if (estado == "atendiendo") {
        document.getElementById("iniciar_atencion").style.display = "none";
        document.getElementById("opciones").style.display = "none";
        document.getElementById("terminar_atencion").style.display = "block";

    } else if (estado == "atendido") {
        document.getElementById("iniciar_atencion").style.display = "block";
        document.getElementById("opciones").style.display = "none";
        document.getElementById("terminar_atencion").style.display = "none";

    } else if (estado == "rechazado") {
        document.getElementById("iniciar_atencion").style.display = "block";
        document.getElementById("opciones").style.display = "none";
        document.getElementById("terminar_atencion").style.display = "none";

    }
}

document.getElementById("btnIniciar").addEventListener("click", () => {
    const veces_llamado = parseInt(document.getElementById("veces_llamado").value) + 1;
    cambiar_estado("llamando", veces_llamado);
})

document.getElementById("btnAtender").addEventListener("click", () => {
    const veces_llamado = document.getElementById("veces_llamado").value;
    cambiar_estado("atendiendo", veces_llamado);
})

document.getElementById("btnVolver").addEventListener("click", () => {
    const veces_llamado = parseInt(document.getElementById("veces_llamado").value) + 1;
    console.log(veces_llamado)
    cambiar_estado("llamando", veces_llamado);
})

document.getElementById("btnSiguiente").addEventListener("click", () => {
    const veces_llamado = document.getElementById("veces_llamado").value;
    cambiar_estado("rechazado", veces_llamado);
})

document.getElementById("btnDetener").addEventListener("click", () => {
    const veces_llamado = document.getElementById("veces_llamado").value;
    cambiar_estado("atendido", veces_llamado);
})

obtener_pendientes();

setInterval(() => {
    obtener_pendientes();
}, 3000)

//obtener_pendientes()