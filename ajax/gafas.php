<?php
session_start();
require_once "../modelos/Gafas.php";
require_once "../vendor/autoload.php";
$gafas = new Gafas();

$_POST = json_decode(file_get_contents("php://input"), true);

$idGafas = isset($_POST['idGafas']) ? limpiarCadena($_POST['idGafas']) : "";
$marca     = isset($_POST['marca']) ? limpiarCadena($_POST['marca']) : "";
$modelo    = isset($_POST['modelo']) ? limpiarCadena($_POST['modelo']) : "";
$color     = isset($_POST['color']) ? limpiarCadena($_POST['color']) : "";
$articulo  = isset($_POST['articulo']) ? limpiarCadena($_POST['articulo']) : "";
$cantidad  = isset($_POST['cantidad']) ? limpiarCadena($_POST['cantidad']) : "";
$sexo      = isset($_POST['sexo']) ? limpiarCadena($_POST['sexo']) : "";
$precioUno = isset($_POST['precioUno']) ? limpiarCadena($_POST['precioUno']) : "";
$precioDos = isset($_POST['precioDos']) ? limpiarCadena($_POST['precioDos']) : "";
$opcion    = isset($_POST['opcion']) ? limpiarCadena($_POST['opcion']) : "";
$archivoExcel = isset($_FILES['excelFileGafas']) ? limpiarCadena($_FILES['excelFileGafas']) : "";
$orden = isset($_POST['orden']) ? limpiarCadena($_POST['orden']) : 1;
switch ($_GET["op"]) {
    case 'guardarFile':
        if (
            $archivoExcel['type'] !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
            $archivoExcel['type'] !== 'application/vnd.ms-excel' &&
            pathinfo($archivoExcel['name'], PATHINFO_EXTENSION) !== 'xlsx' &&
            pathinfo($archivoExcel['name'], PATHINFO_EXTENSION) !== 'xls'
        ) {
            echo "El archivo no es un archivo de Excel válido.";
            exit;
        }

        if (isset($archivoExcel)) {
            $archivoTemporal = $archivoExcel['tmp_name'];
            try {
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($archivoTemporal);
                $spreadsheet = $reader->load($archivoTemporal);
                $hoja = $spreadsheet->getActiveSheet();
                $datosExcel = [];
                $filasVacias = [];
                $chunkSize = 200;
                $highestRow = $hoja->getHighestRow();
                $idGafas = 0;
                for ($startRow = 2; $startRow <= $highestRow; $startRow += $chunkSize) {
                    $range = 'A' . $startRow . ':F' . ($startRow + $chunkSize);
                    $rowData = $hoja->rangeToArray($range, NULL, TRUE, TRUE);

                    foreach ($rowData as $row) {
                        if(!empty($row[3])){
                            $precioUno = isset($row[7]) ? $row[7] : 0.01;
                            $precioDos = isset($row[8]) ? $row[8] : 1;
                            $cantidad = isset($row[4]) ? intval($row[4]) : 0;
                            $rspta = $gafas->insertar($row[0], $row[1], $row[2], $row[3], $cantidad, $row[5], doubleval($precioUno), doubleval($precioDos));
                            $datosExcel[] = $row;
                        }
                    }
                    sleep(3);
                }
                echo json_encode($datosExcel);
                // echo json_encode([
                //     "mensaje" => "Archivo leido",
                //     "datos" => $datosExcel,
                //     "filasVacias" => $filasVacias
                // ]);
            } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
                echo "Error al cargar el archivo: " . $e->getMessage();
                exit;
            }
        } else {
            echo "No se ha recibido ningún archivo.";
        }
        break;
    case 'guardaryeditar':
        if ($idGafas == "") {
            $gafas_insertado = null;
            // $getID = $gafas->getId();
            // if (isset($getID)) {
                // ($getID["idGafas"] + 1), 
                $rspta = $gafas->insertar($marca, $modelo, $color, $articulo, $cantidad, $sexo, $precioUno, $precioDos);
                echo 'Marca:: '. $marca.' - Modelo::  '. $modelo.' - Color:: '. $color.' - Articulo:: '. $articulo.' - Cantidad:: '. $cantidad . ' - Sex:: '.$sexo;
                $gafas_insertado = $rspta ? $gafas->obtener_registrado() : null;
                echo json_encode($gafas_insertado);
            // }
        } else {
            $gafas_editado = null;
            $rspta = $gafas->editar($idGafas, $marca, $modelo, $color, $articulo, $cantidad, $sexo, $precioUno, $precioDos);
            $gafas_editado = $rspta ? $gafas->obtener_registrado($idGafas) : null;
            echo json_encode($gafas_editado);
        }
        break;
    case 'activarydesactivar':
        $rspta = $gafas->activar_desactivar($idGafas, $opcion);
        echo $rspta ? 1 : 0;
        break;
    case 'obtener_data':
        $rspta = $gafas->obtener_data($idGafas);
        echo json_encode($rspta);
        break;
    case 'listar':
        $listaGafas = $gafas->listar($orden);
        $pagination = 0;
        if (isset($listaGafas["tamanio"][0]["numero_de_registros"])) {
            $pagination = ceil($listaGafas["tamanio"][0]["numero_de_registros"] / 10);
        }

        echo json_encode(["lenght" => $listaGafas["tamanio"], "pagination" => $pagination, "info" => $listaGafas["datos"], "page" => $orden]);
        break;
    case 'eliminar':
        echo "ID:: " . $idGafas;
        $gafas_eliminada = null;
        $rspta = $gafas->eliminar($idGafas);
        $gafas_eliminada = $rspta ? $gafas->obtener_registrado() : null;
        echo json_encode($gafas_eliminada);
        break;
}
