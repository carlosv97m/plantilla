<?php
session_start();
require_once "../modelos/Usuario.php";
$usuario = new Usuario();

$_POST = json_decode(file_get_contents("php://input"), true);

switch ($_GET["op"]) {
    case 'guardar':
        $clavehash = hash("SHA256", $password);
        $rspta = $usuario->insertar(
            $nombre,
            $user,
            $clavehash,
            $sexo,
            $telefono,
            $foto,
            $cargo,
            $fecha_registro,
            $fecha_actualizacion,
            $fecha_ultimo_ingreso,
            $activo
        );
        echo $rspta ? "Usuario insertado" : "No se pudieron registrar todos los datos del usuario";
        break;
    case 'editar':
        $clavehash = hash("SHA256", $password);
        $rspta = $usuario->editar(
            $id_usuarios,
            $nombre,
            $user,
            $clavehash,
            $sexo,
            $telefono,
            $foto,
            $cargo,
            $fecha_registro,
            $fecha_actualizacion,
            $fecha_ultimo_ingreso,
            $activo
        );
        echo $rspta ? "Usuario actualizado" : "Usuario no se pudo actualizar";
        break;
    case 'desactivar':
        $rspta = $usuario->desactivar($id_usuarios);
        echo $rspta ? "Usuario desactivado" : "Usuario no se pudo desactivar";
        break;
    case 'activar':
        $rspta = $usuario->activar($id_usuarios);
        echo $rspta ? "Usuario activado" : "Usuario no se pudo activar";
        break;
    case 'mostrar':
        $rspta = $usuario->mostrar($id_usuarios);
        //Codifica el resultado en JSON
        echo json_encode($rspta);
        break;
    case 'listar':
        $rspta = $usuario->listar();
        $data = array();

        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => ($reg->condicion) ? '<button class="btn btn-warning" onclick="mostrar(' . $reg->id_usuarios . ')"><i class="fa fa-pencil"></i></button>' .
                    ' <button class="btn btn-danger" onclick="desactivar(' . $reg->id_usuarios . ')"><i class="fa fa-close"></i></button>' :
                    '<button class="btn btn-warning" onclick="mostrar(' . $reg->id_usuarios . ')"><i class="fa fa-pencil"></i></button>' .
                    ' <button class="btn btn-primary" onclick="activar(' . $reg->id_usuarios . ')"><i class="fa fa-check"></i></button>',
                "1" => $reg->nombre,
                "2" => $reg->tipo_documento,
                "3" => $reg->num_documento,
                "4" => $reg->telefono,
                "5" => $reg->email,
                "6" => $reg->login,
                "7" => '<img src="../files/usuarios/' . $reg->imagen . '" height="50px" width="50px">',
                "8" => ($reg->condicion) ? '<span class="label bg-green">Activado</span>' : '<span class="label bg-red">Desactivado</span>'
            );
        }
        $results = array(
            "sEcho" => 1, //informacion al data table
            "iTotalRecords" => count($data), //total registros datatable
            "iTotalDisplayRecords" => count($data), //total de registros 
            "aaData" => $data
        );
        echo json_encode($results);

        break;
    case "verificar":
        $login = isset($_POST["login"]) ? limpiarCadena($_POST["login"]) : "";
        $clave = isset($_POST["clave"]) ? limpiarCadena($_POST["clave"]) : "";
        $clavehash = hash("SHA512", $clave);

        $fetch = $usuario->verificar($login, $clavehash);


        if (isset($fetch["info"])) {
            $_SESSION["id_usuario"] = $fetch["info"]["id_usuarios"];
            $_SESSION["nombre"] = $fetch["info"]["nombre"];
            $_SESSION["user"] = $fetch["info"]["user"];
            $_SESSION["sexo"] = $fetch["info"]["sexo"];
            $_SESSION["telefono"] = $fetch["info"]["telefono"];
            $_SESSION["foto"] = $fetch["info"]["foto"];
            $_SESSION["cargo"] = $fetch["info"]["cargo"];
        }
        // $roles = $usuario->listamarcados($_SESSION["id_usuario"]);
        echo json_encode($fetch);

        break;

    case "cambiar_contraseña":
        $id_usuarios = isset($_POST['id_usuarios']) ? limpiarCadena($_POST['id_usuarios']) : "";
        $clave = isset($_POST['clave']) ? limpiarCadena($_POST['clave']) : "";
        $rspta = $usuario->cambiar_contraseña($id_usuarios, $clave);
        echo $rspta == 1 ? 1 : 0; // 0 => Fallo 1=> Guardo
        break;
    case "salir":
        session_unset();
        session_destroy();
        header("Location: ../index.php");
        break;
}
