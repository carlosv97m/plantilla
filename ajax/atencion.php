<?php
session_start();
require_once "../modelos/Atencion.php";
$atencion = new Atencion();
function solicitudes(){
    $sql = "SELECT * FROM ticket";
    return ejecutarConsulta($sql);
}
$_POST = json_decode(file_get_contents("php://input"), true);

switch ($_GET["op"]) {
    case 'guardaryeditar':
        if (empty($id_ticket)) {
            $rspta = $atencion->insertar(
                $codigo_ticket,
                $fecha,
                $hora,
                $id_conf_tipo_colas,
                $estado_ticket,
                $veces_llamado,
                $activo
            );
            echo $rspta ? "Ticket impreso" : "No se pudo imprimir el ticker";
        }
        break;
    case 'listar':
        $rspta = $atencion->listar();
        $data = array();

        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => $reg->codigo_ticket,
                "1" => $reg->fecha,
                "2" => $reg->hora,
                "3" => $reg->id_conf_tipo_colas,
                "4" => $reg->estado_ticket,
                "5" => ($reg->activo) ? '<span class="label bg-green">Activado</span>' : '<span class="label bg-red">Desactivado</span>'
            );
        }
        $results = array(
            "sEcho" => 1, //informacion al data table
            "iTotalRecords" => count($data), //total registros datatable
            "iTotalDisplayRecords" => count($data), //total de registros 
            "aaData" => $data
        );
        echo json_encode($results);

        break;
    case 'solicitudes':
        $fecha = isset($_GET['fecha']) ? limpiarCadena($_GET['fecha']) :'';
        $hora = isset($_GET['hora']) ? limpiarCadena($_GET['hora']) :'';
        $rspta = $atencion->solicitudes($fecha);
        $data = array();
        $info = array();
        foreach($rspta as  $key)
        {
            $data = array(
                "0" => $key['codigo_ticket'],
                "1" => $key['fecha'],
                "2" => $key['hora'],
                "3" => $key['estado_ticket'],
                "4" => $key['id_conf_tipo_colas'],
                "5" => $key['veces_llamado'],
            );
            array_push($info, $data);
        }
        $results = array(
            "sEcho" => 1, //informacion al data table
            "iTotalRecords" => count($info), //total registros datatable
            "iTotalDisplayRecords" => count($info), //total de registros 
            "aaData" => $info
        );
        echo json_encode($info);
        // print_r($rspta);
        break;
    case "salir":
        session_unset();
        session_destroy();
        header("Location: ../index.php");
        break;
}
