<?php 
    session_start();
    require_once "../modelos/Mensaje.php";
    $mensaje = new Mensaje();
    $_POST = json_decode(file_get_contents("php://input"), true);

    $id_mensaje = isset($_POST['id_mensaje']) ? limpiarCadena($_POST['id_mensaje']) : "";
    $msj        = isset($_POST['mensaje']) ? limpiarCadena($_POST['mensaje']) : "";
    $mensaje_editado = null;

    switch ($_GET["op"]) {
        case "guardaryeditar":
            // echo $id_mensaje. " -- ". $msj;
            if ($id_mensaje == 0) {
                // echo "awui".$mensaje;
                $rspta = $mensaje->crear_mensaje($msj);
                $mensaje_insertado = $rspta ? $mensaje->obtener_registrado() : null;
                echo json_encode(["info" => $rspta["info"], "mensaje" => $rspta["message"]]);

            } else {
                $rspta = $mensaje->editar_mensaje($id_mensaje, $msj);
                // $mensaje_editado = $rspta ? $mensaje->obtener_registrado($id_mensaje) : null;
                echo json_encode(["info" => $rspta["info"], "mensaje" => $rspta["message"]]);
            }
            break;
        case "listar_mensaje":
            $listar_mensaje = $mensaje->mostrar_mensaje();
            if($listar_mensaje) { echo json_encode($listar_mensaje); } else { echo json_encode([]); }
            break;
    }
?>