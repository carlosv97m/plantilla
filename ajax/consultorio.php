<?php
session_start();
require_once "../modelos/Consultorio.php";
$consultorio = new Consultorio();

$_POST = json_decode(file_get_contents("php://input"), true);

switch ($_GET["op"]) {
    case 'listar_pendientes':
        date_default_timezone_set('America/La_Paz');
        $fecha = date("Y-m-d");
        $id_cola = isset($_POST["id_cola"]) ? limpiarCadena($_POST["id_cola"]) : "";
        $atender = $consultorio->listar_pendientes($id_cola, $fecha);
        $num_cola = $consultorio->contar_cola($id_cola, $fecha);
        $resultado = [
            "num_cola" => $num_cola["num_pacientes"],
            "atender" => $atender,
        ];
        echo json_encode($resultado);
        break;
    case 'cambiar_estado':
        $id_ticket = isset($_POST["id_ticket"]) ? limpiarCadena($_POST["id_ticket"]) : "";
        $estado = isset($_POST["estado"]) ? limpiarCadena($_POST["estado"]) : "";
        $veces_llamado = isset($_POST["veces_llamado"]) ? limpiarCadena($_POST["veces_llamado"]) : 0;
        $rspta = $consultorio->cambiar_estado($id_ticket, $estado, $veces_llamado);
        echo $rspta ? 1 : 0;
        break;
}
