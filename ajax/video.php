<?php
session_start();
require_once "../modelos/Video.php";
$video = new Video();

$_POST = json_decode(file_get_contents("php://input"), true);

$id_video = isset($_POST['id_video']) ? limpiarCadena($_POST['id_video']) : "";
$nombre = isset($_POST['nombre']) ? limpiarCadena($_POST['nombre']) : "";
$link = isset($_POST['link']) ? limpiarCadena($_POST['link']) : "";
$opcion = isset($_POST['opcion']) ? limpiarCadena($_POST['opcion']) : "";

switch ($_GET["op"]) {
    case 'guardaryeditar':
        if ($id_video == "") {
            $video_insertado = null;
            $rspta = $video->insertar($nombre, $link);
            $video_insertado = $rspta ? $video->obtener_registrado() : null;
            echo json_encode($video_insertado);
        } else {
            $video_editado = null;
            $rspta = $video->editar($id_video, $nombre, $link);
            $video_editado = $rspta ? $video->obtener_registrado($id_video) : null;
            echo json_encode($video_editado);
        }
        break;
    case 'activarydesactivar':
        $rspta = $video->activar_desactivar($id_video, $opcion);
        echo $rspta ? 1 : 0;
        break;
    case 'obtener_links':
        $rspta = $video->obtener_links();
        echo json_encode($rspta);
        break;
    case 'listar':
        $lista_videos = $video->listar();
        echo json_encode(["info" => $lista_videos["info"], "message" => $lista_videos["message"]]);
        break;
    case 'eliminar':
        $rspta = $video->eliminar($id_video);
        echo $rspta ? 1 : 0;
        break;
}
