<?php
session_start();
require_once "../modelos/Ticket.php";
$ticket = new Ticket();

$_POST = json_decode(file_get_contents("php://input"), true);

switch ($_GET["op"]) {
    case 'guardar':
        $codigo_ticket = isset($_POST["codigo_ticket"]) ? limpiarCadena($_POST["codigo_ticket"]) : "";
        $fecha = isset($_POST["fecha"]) ? limpiarCadena($_POST["fecha"]) : "";
        $hora = isset($_POST["hora"]) ? limpiarCadena($_POST["hora"]) : "";
        $id_conf_tipo_colas = isset($_POST["id_cola"]) ? limpiarCadena($_POST["id_cola"]) : "";
        $rspta = $ticket->insertar(
            $codigo_ticket,
            $fecha,
            $hora,
            $id_conf_tipo_colas
        );
        echo $rspta == 1 ? 1 : 0; // 0 => Fallo 1=> Guardo
        break;
    case 'listar_pendientes':

        //$rspta = $ticket->listar();


        // while ($reg = $rspta->fetch_object()) {
        //     $data[] = array(
        //         "0" => $reg->codigo_ticket,
        //         "1" => $reg->fecha,
        //         "2" => $reg->hora,
        //         "3" => $reg->id_conf_tipo_colas,
        //         "4" => $reg->estado_ticket,
        //         "5" => ($reg->activo) ? '<span class="label bg-green">Activado</span>' : '<span class="label bg-red">Desactivado</span>'
        //     );
        // }
        // $results = array(
        //     "sEcho" => 1, //informacion al data table
        //     "iTotalRecords" => count($data), //total registros datatable
        //     "iTotalDisplayRecords" => count($data), //total de registros 
        //     "aaData" => $data
        // );
        // echo json_encode($results);

        break;
    case 'verificar_ticket':
        $fecha_actual = isset($_GET["fecha"]) ? limpiarCadena($_GET["fecha"]) : "";
        $rspta = $ticket->verificar_ticket($fecha_actual);
        echo $rspta["verificado"];
        break;
}
