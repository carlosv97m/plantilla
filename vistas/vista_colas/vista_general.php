 <!-- <div class="main-content"> -->
 <div class="section__content section__content--p30">
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-12">
                 <div class="overview-wrap">
                     <h2 class="title-1"><?php echo $_SESSION['cargo'] ?></h2>

                 </div>
             </div>
         </div>
         <p class="display-6">
             Por atender: <b id="num_cola"> </b> pacientes
         </p>
         <div class="row ">
             <table class="table table-borderless  table-striped2 table-earning3 w-25 mr-2 ">
                 <thead class="bg-success ">
                     <tr>
                         <th>
                             Atendiendo
                         </th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <td>
                             <input type="hidden" id="veces_llamado">
                             <input type="hidden" id="atendiendo_id">
                             <div id="atendiendo"></div>
                         </td>
                     </tr>
                 </tbody>
             </table>
             <table class="table table-borderless table-striped2 table-earning2 w-25">
                 <thead class="bg-dark">
                     <tr>
                         <th>
                             Siguiente
                         </th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <td id="siguiente">
                         </td>
                     </tr>
                 </tbody>
             </table>
         </div>
         <div class="row pt-3 ">
             <div class="col-sm-9 col-lg-9  ">
                 <div id="iniciar_atencion">
                     <button type="button" class="btn overview-item--c7 rounded" id="btnIniciar" style="font-size:25px; margin:5px;">
                         <i class="fas fa-play"></i>
                         Iniciar Atención
                     </button>
                 </div>
                 <div class="opciones" id="opciones" style="display:none;">
                     <button type="button" class="btn overview-item--c7 rounded" id="btnAtender" style="font-size:25px;margin:5px;">

                         <i class="fas fa-syringe"></i>
                         Atender
                     </button>
                     <button type="button" class="btn overview-item--c5 rounded" id="btnVolver" style="font-size:25px;margin:5px;">
                         <i class="fas fa-bullhorn"></i>
                         Volver a llamar
                     </button>
                     <button type="button" class="btn overview-item--c1 rounded" id="btnSiguiente" style="font-size:25px;margin:5px;">
                         <i class="fas fa-arrow-alt-circle-right"></i>
                         Saltar Ticket
                     </button>
                 </div>
                 <div id="terminar_atencion">
                     <button type="button" class="btn overview-item--c6 rounded" id="btnDetener" style="font-size:25px; margin:5px;">
                         <i class="fas fa-next"></i>
                         Siguiente
                     </button>
                 </div>

             </div>
         </div>


     </div>
 </div>
 <!-- </div> -->