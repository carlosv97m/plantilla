<div class="container">

    <!-- tabla  -->
    <div class="row w-100">
        <h2>
            Personal
        </h2>
        <div class="p-2 col d-flex justify-content-end z-index">
            <!--  modal -->
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                Añadir
            </button>

            <!-- Modal -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Añadir Personal</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <!-- Formulario -->
                            <form class="needs-validation" novalidate method="POST" action="">
                                <div class="form-row">
                                    <div class="col-md-7 mb-3">
                                        <label for="validationCustom01">Nombre</label>
                                        <input type="text" class="form-control" id="validationCustom01" placeholder="Nombre completo" required>

                                        <div class="invalid-feedback">
                                            Ingrese un Nombre Ej. Juan Perez Torres
                                        </div>
                                    </div>

                                    <div class="col-md-5 mb-3">
                                        <label for="validationCustomUsername">Nombre Usuario</label>
                                        <div class="input-group">

                                            <input type="text" class="form-control" id="validationCustomUsername" placeholder="Nombre de Usuario" aria-describedby="inputGroupPrepend" required>
                                            <div class="invalid-feedback">
                                                Por favor elija un nombre de usuario.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom03">Cargo</label>
                                        <!-- <input type="text" class="form-control" id="validationCustom03" placeholder="City" required> -->
                                        <Select class="custom-select" id="validationCustom03" required>
                                            <option value="">--Elija una Opcion--</option>
                                            <option value="voluntario">Voluntario</option>
                                            <option value="fidelizado">Fidelizado</option>
                                            <option value="distribucion">Distribucion</option>
                                        </Select>
                                        <div class="invalid-feedback">
                                            Por favor elija un cargo.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom04">Celular</label>
                                        <input type="number" class="form-control" id="validationCustom04" placeholder="Ej. 73426611" min="10000000" max="99999999" required>
                                        <div class="invalid-feedback">
                                            Por favor ingrese un celular valido.
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  data-dismiss="modal">Cerrar</button>
                                    <button class="btn btn-primary" type="submit" onclick="registrar_usuario()" >Registrar</button>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="table-responsive table-responsive-data2 table-sm">
            <table class="table table-data2">
                <thead>
                    <tr>

                        <th>N°</th>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>Cargo</th>
                        <th>Telefono</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-shadow">
                        <td>1</td>
                        <td>Pedro Perez</td>
                        <td>fidelizado</td>
                        <td>Enfermero</td>
                        <td>71123486</td>

                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>2</td>
                        <td>Juan Perez</td>
                        <td>voluntario</td>
                        <td>Medico general</td>
                        <td>71123456</td>

                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                </tbody>
            </table>
        </div>

    </div>
    <!--  -->

</div>