<div class="container">


    <div class="row">

        <div class="col-sm-3">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Videos del monitor</a>
                <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Mensaje del monitor</a>
                <a class="nav-link" id="v-pills-gafas-tab" data-toggle="pill" href="#v-pills-gafas" role="tab" aria-controls="v-pills-monturas" aria-selected="false">Gafas</a>
                <a class="nav-link" id="v-pills-paciente-tab" data-toggle="pill" href="#v-pills-paciente" role="tab" aria-controls="v-pills-monturas" aria-selected="false">Pacientes</a>

            </div>
        </div>
        <div class="col-sm-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <?php require "admin/config_video.php" ?>
                </div>
                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                    <?php require "admin/mensaje.php" ?>
                </div>
                <div class="tab-pane fade" id="v-pills-gafas" role="tabpanel" aria-labelledby="v-pills-gafas-tab">
                    <?php require "admin/gafas.php" ?>
                </div>
                <div class="tab-pane fade" id="v-pills-paciente" role="tabpanel" aria-labelledby="v-pills-paciente-tab">
                    <?php require "admin/paciente.php" ?>
                </div>
            </div>
        </div>
    </div>


</div>