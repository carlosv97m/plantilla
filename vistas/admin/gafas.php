<div class="container">
    <div class="row w-100">
        <h2>
            Gafas
        </h2>
        <div class="p-2 col d-flex justify-content-end">
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#UpFileGafas">
                Subir archivo
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalGafas">
                Crear nuevo registro
            </button>
        </div>
        <div class="table-responsive table-responsive-data2 table-sm" style="height: 550px; overflow-y: auto;">
            <table class="table table-data2 ">
                <thead>
                    <tr>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Color</th>
                        <th scope="col">Articulo</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Genero</th>
                        <th scope="col">Fecha de Registro</th>
                        <th scope="col">Precio Uno</th>
                        <th scope="col">Precio Dos</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="contenedor_gafas">
                </tbody>
            </table>
        </div>
        <div class="align-right" style="text-align: right;">
            <nav aria-label="..." style="text-align: right;">
                <ul class="pagination justify-content-end">
                    <li class="page-item">
                        <a class="page-link" href="#" onclick="prevPageGafas()" aria-label="Previous">
                            <span aria-hidden="true">«</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <!-- <strong id="inicio"></strong>&nbsp;de&nbsp; -->
                        <a class="page-link" href="#" style="width: 250px;"><strong id="desdeGafas"></strong>&nbsp;de&nbsp;<strong id="hastaGafas"></strong>&nbsp; <strong>Registros</strong></a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#" onclick="nextPageGafas()" aria-label="Next">
                            <span aria-hidden="true">»</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<input type="hidden" id="initGafas" value="1" min="1">
<input type="hidden" id="totalPagesGafas" value="">
<input type="hidden" id="currentPageGafas" value="">
<!-- Upload File -->
<div class="modal fade" id="UpFileGafas">
    <br><br><br><br><br><br><br><br>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Encabezado del modal -->
            <div class="modal-header">
                <h4 class="modal-title">Subir archivo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Cuerpo del modal -->
            <div class="modal-body">
                <form enctype="multipart/form-data" id="form_file_gafas" class="needs-validation" novalidate>
                    <div class="form-row">
                        <div class="col">
                            <label for="excelFileGafas">Subir archivo</label>
                            <input type="file" id="excelFileGafas" name="excelFileGafas" accept=".xlsx, .xls" required>
                        </div>
                    </div>
                    <div class="modal-body d-flex justify-content-center align-items-center">
                        <button type="submit" style="align-items: center;" class="btn btn-warning mt-3">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Create -->
<div class="modal fade" id="myModalGafas">
    <br><br><br><br><br><br><br><br>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Encabezado del modal -->
            <div class="modal-header">
                <h4 class="modal-title">Formulario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Cuerpo del modal -->
            <div class="modal-body">
                <form id="form_gafas" class="needs-validation" novalidate>
                    <div class="form-row">
                        <div class="col">
                            <input type="text" name="nombre_marca" id="nombre_marca" class="form-control" placeholder="Marca" required>
                        </div>
                        <div class="col">
                            <input type="text" name="nombre_modelo" id="nombre_modelo" class="form-control" placeholder="Modelo" required>
                        </div>
                        <div class="col">
                            <input type="text" name="nombre_color" id="nombre_color" class="form-control" placeholder="Color" required>
                        </div>
                        <div class="col">
                            <input type="text" name="nombre_articulo" id="nombre_articulo" class="form-control" value="GAFAS" required>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col">
                            <input type="number" name="nombre_cantidad" id="nombre_cantidad" class="form-control" placeholder="Cantidad" min="0" required>
                        </div>
                        <div class="col">
                            <select name="nombre_sexo" id="nombre_sexo" class="form-control" required>
                                <option value="" disabled>__SELECCIONE__</option>
                                <option value="V">Varón</option>
                                <option value="M">Mujer</option>
                            </select>
                        </div>
                        <div class="col">
                            <input type="number" name="nombre_precioUno" id="nombre_precioUno" class="form-control" placeholder="Precio Referencia 1" min="0" step="0.01" required>
                        </div>
                        <div class="col">
                            <input type="number" name="nombre_precioDos" id="nombre_precioDos" class="form-control" placeholder="Precio Referencia 2" min="0" step="0.01" required>
                        </div>
                    </div>
                    <div class="modal-body d-flex justify-content-center align-items-center">
                        <button type="submit" style="align-items: center;" class="btn btn-primary mt-3">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit -->
<div class="modal fade" id="myModalGafasEdit">
    <br><br><br><br><br><br><br><br>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Encabezado del modal -->
            <div class="modal-header">
                <h4 class="modal-title">Modificar Registro</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Cuerpo del modal -->
            <div class="modal-body">
                <form id="form_edit_gafas" class="needs-validation" novalidate>
                    <input type="hidden" name="idGafasEdit" id="idGafasEdit">
                    <div class="form-row">
                        <div class="col">
                            <input type="text" name="edit_gafas_marca" id="edit_gafas_marca" class="form-control" placeholder="Marca" required>
                        </div>
                        <div class="col">
                            <input type="text" name="edit_gafas_modelo" id="edit_gafas_modelo" class="form-control" placeholder="Modelo" required>
                        </div>
                        <div class="col">
                            <input type="text" name="edit_gafas_color" id="edit_gafas_color" class="form-control" placeholder="Color" required>
                        </div>
                        <div class="col">
                            <input type="text" name="edit_gafas_articulo" id="edit_gafas_articulo" class="form-control" value="GAFAS" required>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col">
                            <input type="number" name="edit_gafas_cantidad" id="edit_gafas_cantidad" class="form-control" placeholder="Cantidad" min="0" required>
                        </div>
                        <div class="col">
                            <select name="edit_gafas_sexo" id="edit_gafas_sexo" class="form-control" required>
                                <option value="" disabled>__SELECCIONE__</option>
                                <option value="V">Varón</option>
                                <option value="M">Mujer</option>
                            </select>
                        </div>
                        <div class="col">
                            <input type="number" name="edit_gafas_precioUno" id="edit_gafas_precioUno" class="form-control" placeholder="Precio Referencia 1" min="0" step="0.01" required>
                        </div>
                        <div class="col">
                            <input type="number" name="edit_gafas_precioDos" id="edit_gafas_precioDos" class="form-control" placeholder="Precio Referencia 2" min="0" step="0.01" required>
                        </div>
                    </div>
                    <div class="modal-body d-flex justify-content-center align-items-center">
                        <button type="submit" style="align-items: center;" class="btn btn-primary mt-3">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Delete -->
<div class="modal fade" id="myModalDeleteGafas">
    <br><br><br><br><br><br><br><br>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="text-align: center;">

                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center">
                <form id="form_gafas_delete" class="needs-validation" novalidate>
                    <h4 class="modal-title">Alerta</h4>
                    <p style="align-items: center;">
                        <strong>Esta seguro de eliminar el registro?</strong>
                    </p>
                    <input type="hidden" name="idGafasDelete" id="idGafasDelete">
                    <div class="modal-body d-flex justify-content-center align-items-center">
                        <button type="submit" style="align-items: center;" class="btn btn-danger mt-3">Si, eliminar!</button>&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-warning mt-3" onclick="deleteGafasClose()">No, cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>