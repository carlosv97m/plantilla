<div class="container">
    <div class="row w-100">
        <h2>
            Configuracion del monitor
        </h2>
        <div class="p-2 col d-flex justify-content-end">


        </div>


        <div class="p-2">
            <form id="form_video" class="needs-validation form-inline" novalidate>
                <div class="form-group mb-2">
                    <label for="staticEmail2" class="sr-only">Nombre Video</label>
                    <input type="text" class="form-control" id="nombre_video" name="nombre_video" placeholder="Nombre" required>
                </div>
                <!-- <input type="hidden" name="id_video" id="id_video" value=1> -->
                <div class="invalid-feedback">
                    Nombre Requerido
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="inputPassword2" class="sr-only">Link Video</label>
                    <input type="url" class="form-control" id="link_video" name="link_video" placeholder="Link del Video" required>
                </div>
                <div class="invalid-feedback">
                    Nombre Requerido
                </div>
                <button type="submit" class="btn btn-primary mb-2">Guardar</button>
            </form>
        </div>
        <div class="table-responsive table-responsive-data2 table-sm">
            <table class="table table-data2 ">
                <thead>
                    <tr>
                        <th scope="col">HABILITADO</th>
                        <th scope="col">Nombre Video</th>
                        <th scope="col">URL</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="contenedor_videos">
                </tbody>
            </table>
        </div>

    </div>
</div>