<?php
date_default_timezone_set('America/La_Paz');
?>
<div class="container">
    <div class="row w-100">
        <h2>
            Pacientes
        </h2>
        <div class="p-2 col d-flex justify-content-end">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#registerPacient">
                Registrar Paciente
            </button>
        </div>
        <nav class="navbar navbar-light justify-content-end">
            <input class="searchbox form-control mr-sm-2" type="search" style="text-align: right;" placeholder="Search" aria-label="Search">
        </nav>
        <div class="table-responsive table-responsive-data2 table-sm" style="height: 550px; overflow-y: auto;">
            <table class="table table-data2 ">
                <thead>
                    <tr>
                        <th scope="col">Paciente</th>
                        <th scope="col">CI</th>
                        <th scope="col">Numeor de Historia</th>
                        <th scope="col">Ocupacion</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Motivo</th>
                        <th scope="col">Fecha de Registro</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="contenedor_pacientes">
                </tbody>
            </table>
        </div>
        <div class="align-right" style="text-align: right;" class="justify-content-end">
            <nav aria-label="..." style="text-align: right;">
                <ul class="pagination justify-content-end">
                    <li class="page-item">
                        <a class="page-link" href="#" onclick="prevPagePaciente()" aria-label="Previous">
                            <span aria-hidden="true">«</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <!-- <strong id="inicio"></strong>&nbsp;de&nbsp; -->
                        <a class="page-link" href="#" style="width: 250px;"><strong id="desdePaciente"></strong>&nbsp;de&nbsp;<strong id="hastaPaciente"></strong>&nbsp; <strong>Registros</strong></a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#" onclick="nextPagePaciente()" aria-label="Next">
                            <span aria-hidden="true">»</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<input type="hidden" id="initPaciente" value="1" min="1">
<input type="hidden" id="totalPagesPaciente" value="">
<input type="hidden" id="currentPagePaciente" value="">

<!-- Create -->
<div class="modal fade" id="registerPacient">
    <br><br><br><br><br>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Encabezado del modal -->
            <div class="modal-header">
                <h4 class="modal-title">Formulario de Registro</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Cuerpo del modal -->
            <div class="modal-body">
                <form id="form_paciente" class="needs-validation" novalidate>
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Registro Personal
                                    </button>
                                </h2>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="form-row" style="display: none;">
                                        <div class="col">
                                            <label for="fechaRegistro">Fecha Regsitro</label>
                                            <input type="datetime-local" name="fechaRegistro" id="fechaRegistro" class="form-control" value="<?php echo date("Y-m-d\TH:i") ?>" required>
                                        </div>
                                        <div class="col">
                                            <label for="fechaPrimeraConsulta">Fecha Primera Consulta</label>
                                            <input type="date" name="fechaPrimeraConsulta" id="fechaPrimeraConsulta" class="form-control" value="<?php echo date("Y-m-d") ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="primerApellido">Apellido Paterno</label>
                                            <input type="text" name="primerApellido" id="primerApellido" class="form-control" placeholder="Apellido Paterno" required>
                                        </div>
                                        <div class="col">
                                            <label for="segundoApellido">Apellido Materno</label>
                                            <input type="text" name="segundoApellido" id="segundoApellido" class="form-control" placeholder="Apellido Materno" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="nombre">Nombre(s)</label>
                                            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre(s)" required>
                                        </div>
                                        <div class="col">
                                            <label for="apellidoEsposo">Apellido de Esposo(a)</label>
                                            <input type="text" name="apellidoEsposo" id="apellidoEsposo" class="form-control" placeholder="Apellido de Esposo(a)" value="-" required>
                                        </div>
                                        <div class="col">
                                            <label for="idEstadoCivil">Estado Civil</label>
                                            <select name="idEstadoCivil" id="idEstadoCivil" class="form-control" required>
                                                <option value="" disabled selected>__SELECCIONE__</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="ci">Ci</label>
                                            <input type="text" name="ci" id="ci" class="form-control" placeholder="Carnet de Identidad" required>
                                        </div>
                                        <div class="col">
                                            <label for="genero_paciente">Genero</label>
                                            <select name="genero_paciente" id="genero_paciente" class="form-control" required>
                                                <option value="" disabled selected>__SELECCIONE__</option>
                                                <option value="V">Varón</option>
                                                <option value="M">Mujer</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="fechaNacimiento">Fecha de Nacimiento</label>
                                            <input type="date" name="fechaNacimiento" id="fechaNacimiento" class="form-control" required>
                                        </div>
                                        <input type="hidden" name="usuarioRegistro" id="usuarioRegistro" value="<?php echo $_SESSION["nombre"] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Sobre el Cliente
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="numeroHistoria">Numero de Historia</label>
                                            <input type="text" name="numeroHistoria" id="numeroHistoria" class="form-control" placeholder="Numero de Historia" required>
                                        </div>
                                        <div class="col">
                                            <label for="ocupacion">Ocupacion</label>
                                            <input type="text" name="ocupacion" id="ocupacion" class="form-control" placeholder="Ocupacion" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="direccion">Direccion</label>
                                            <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Direccion" required>
                                        </div>
                                        <div class="col">
                                            <label for="idProcedencia">Procedencia</label>
                                            <select name="idProcedencia" id="idProcedencia" class="form-control" required>
                                                <option value="" disabled selected>__SELECCIONE__</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="telefono">Telefono</label>
                                            <input type="text" name="telefono" id="telefono" class="form-control" placeholder="Numero de Telefono Fijo" required>
                                        </div>
                                        <div class="col">
                                            <label for="celular">Celular</label>
                                            <input type="text" name="celular" id="celular" class="form-control" placeholder="Numero de Celular" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="email">Correo Electronico</label>
                                            <input type="text" name="email" id="email" class="form-control" placeholder="Correo Electronico" required>
                                        </div>
                                        <div class="col">
                                            <label for="idInstitucion">Institucion</label>
                                            <select name="idInstitucion" id="idInstitucion" class="form-control" required>
                                                <option value="" disabled selected>__SELECCIONE__</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="observacion">Observaciones sobre el cliente</label>
                                            <textarea name="observacion" id="observacion" class="form-control" placeholder="Observaciones" cols="5" rows="5">NINGUNO</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Motivo
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="planTrabajo">Plan de Trabajo</label>
                                            <input type="text" name="planTrabajo" id="planTrabajo" class="form-control" placeholder="Plan de Trabajo" value="Uso de Lentes" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="motivo">Motivo de la Visita</label>
                                            <textarea name="motivo" id="motivo" class="form-control" cols="5" rows="5">Cotización de Lentes</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body d-flex justify-content-center align-items-center">
                        <button type="submit" style="align-items: center;" class="btn btn-primary mt-3">Guardar</button>
                        <button type="button" class="btn btn-warning mt-3" onclick="cancelarRegistroPaciente()">Borrar campos</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Cotizacion -->
<div class="modal fade" id="ModalPostQuote">
    <br><br><br><br><br>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registro de Cotización</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form_paciente" class="needs-validation" novalidate>

                    <div class="row">
                        <div class="col-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <button class="nav-link active" id="v-pills-ccristales-tab" data-toggle="pill" data-target="#v-pills-ccristales" type="button" role="tab" aria-controls="v-pills-ccristales" aria-selected="true">Cristales</button>
                                <button class="nav-link" id="v-pills-cmontura-tab" data-toggle="pill" data-target="#v-pills-cmontura" type="button" role="tab" aria-controls="v-pills-cmontura" aria-selected="false">Montura</button>
                                <button class="nav-link" id="v-pills-cgafas-tab" data-toggle="pill" data-target="#v-pills-cgafas" type="button" role="tab" aria-controls="v-pills-cgafas" aria-selected="false">Gafas</button>
                                <button class="nav-link" id="v-pills-cotizacion-tab" data-toggle="pill" data-target="#v-pills-cotizacion" type="button" role="tab" aria-controls="v-pills-cotizacion" aria-selected="false">Cotizacion</button>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-ccristales" role="tabpanel" aria-labelledby="v-pills-ccristales-tab">
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="idCTCristales">Tipo de Vision</label>
                                            <select name="idCTCristales" id="idCTCristales" class="form-control" required>
                                                <option value="" disabled selected>__SELECCIONE__</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="categoriaCristal">Categoria</label>
                                            <select name="categoriaCristal" id="categoriaCristal" class="form-control" required>
                                                <option value="" disabled selected>__SELECCIONE__</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <div class="col ojo">
                                            **Ojo derecho**
                                            <div class="form-group">
                                                <label for="idCristalOD">Cristal OD</label>
                                                <select name="idCristalOD" id="idCristalOD" class="form-control">
                                                    <option value="" disabled selected>__SELECCIONE__</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col ojo">
                                            **Ojo izquierdo**
                                            <div class="form-group">
                                                <label for="idCristalOI">Cristal OI</label>
                                                <select name="idCristalOI" id="idCristalOI" class="form-control">
                                                    <option value="" disabled selected>__SELECCIONE__</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-cmontura" role="tabpanel" aria-labelledby="v-pills-cmontura-tab">
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="idMontura">Montura</label>
                                            <select name="idMontura" id="idMontura" class="form-control" required>
                                                <option value="" disabled selected>__SELECCIONE__</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="obsMontura">Observaciones</label>
                                            <input type="text" name="obsMontura" id="obsMontura" class="form-control" placeholder="Observaciones" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-cgafas" role="tabpanel" aria-labelledby="v-pills-cgafas-tab">
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="idGafas">Gafas</label>
                                            <select name="idGafas" id="idGafas" class="form-control" required>
                                                <option value="" disabled selected>__SELECCIONE__</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="obsGafas">Observaciones</label>
                                            <input type="text" name="obsGafas" id="obsGafas" class="form-control" placeholder="Observaciones" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-cotizacion" role="tabpanel" aria-labelledby="v-pills-cotizacion-tab">...</div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body d-flex justify-content-center align-items-center">
                        <button type="submit" style="align-items: center;" class="btn btn-primary mt-3">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .navbar .searchbox {
        width: 200px;
        /* Ajusta este valor según el tamaño que desees */
        margin-left: auto;
        /* Alinea el elemento a la derecha */
    }

    .color-rojo {
        background-color: red;
    }

    .color-verde {
        background-color: green;
    }

    .color-azul {
        background-color: blue;
    }

    .ojo {
        background-color: #f7f7f7;
        padding: 10px;
        border-radius: 5px;
        margin: 0 auto;
        width: 50%;
        text-align: center;
    }
</style>