<div class="container">

    <!-- tabla  -->
    <form id="form_mensaje" class="needs-validation" novalidate method="POST" action="">
        <div class="row w-100">
            <h2>
                Mensaje
            </h2>
            <div class="p-2 col d-flex justify-content-end z-index">
                <!--  modal -->
                <!-- Button trigger modal -->
                <button type="submit" class="btn btn-primary">
                    Guardar
                </button>

                <!-- Modal -->

            </div>
            <div class="p-2 col-12">
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label for="validationCustom01">Introduzca un mensaje</label>
                        <textarea type="text" class="form-control" id="mensajeMonitor" name="mensajeMonitor" placeholder="Mensaje del monitor" autofocus maxlength="250" rows=8 required>
                        </textarea>
                        <div class="invalid-feedback">
                            Ingrese un mensaje que se mostrara en el monitor
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-2 col-12">
                <div class="form-row">
                    <input type="hidden" name="id_mensaje" id="id_mensaje">
                </div>
            </div>




        </div>
    </form>
    <!--  -->

</div>