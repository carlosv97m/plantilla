  <!DOCTYPE html>
  <html lang="en">

  <head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Banco de Sangre</title>

    <!-- Fontfaces CSS-->
    <link href="../public/css/font-face.css" rel="stylesheet" media="all">
    <link href="../public/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../public/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../public/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../public/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../public/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="../public/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../public/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="../public/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../public/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="../public/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../public/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="../public/vendor/vector-map/jqvmap.min.css" rel="stylesheet" media="all">
    <!-- <link href="../public/vendor/vector-map/jqvmap.min.css" rel="stylesheet" media="all"> -->

    <!-- Main CSS-->
    <?php if ($_SESSION["cargo"] == "monitor") { ?>
      <link href="../public/css/monitor.css" rel="stylesheet" media="all">
    <?php } else { ?>
      <link href="../public/css/theme.css" rel="stylesheet" media="all">
    <?php }; ?>

  </head>

  <body class="animsition">
    <?php
    /* imagen */
    $img_dir = "logo bsch.png";
    /* informacion de los menus */
    $link1 = "Tickets";
    $link2 = "Configuracion Tickets";
    $link3 = "Configuracion de colas";
    $link4 = "Inicio";
    $link5 = "Personal";
    if ($_SESSION["sexo"] == "masculino") {
      $imagen_usuario = "icon/docH.png";
    } else {
      $imagen_usuario = "icon/docM.png";
    }


    if ($_SESSION["cargo"] != "monitor" && $_SESSION["cargo"] != "ticket") { ?>
      <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
          <div class="section__content section__content--p35">
            <div class="header3-wrap">
              <div class="header__logo">
                <a href="#">
                  <img src="../public/images/icon/logo-bsch-rectangular.png" alt="Banco de Sangre" width="70%" />
                </a>
              </div>

              <!--  -->
              <div class="header__tool">
                <div class="account-wrap">
                  <div class="account-item account-item--style2 clearfix js-item-menu">
                    <div class="image">
                      <img src="../public/images/<?php echo $imagen_usuario; ?>" alt="John Doe" />
                    </div>
                    <div class="content">
                      <a class="js-acc-btn text-dark" href="#"><?php echo $_SESSION["nombre"] ?></a>
                    </div>
                    <div class="account-dropdown js-dropdown">
                      <div class="info clearfix">
                        <div class="image">
                          <a href="#">
                            <img src="../public/images/<?php echo $imagen_usuario; ?>" alt="John Doe" />
                          </a>
                        </div>
                        <div class="content">
                          <h5 class="name">
                            <a href="#"><?php echo $_SESSION["nombre"] ?></a>
                          </h5>
                          <span class="email"><?php echo $_SESSION["cargo"] ?></span>
                        </div>
                      </div>

                      <div class="account-dropdown__footer">
                        <a href="../ajax/usuario.php?op=salir">
                          <i class="zmdi zmdi-power"></i>Salir</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <?php if (!($_SESSION["cargo"] == "monitor" || $_SESSION["cargo"] == "ticket")) { ?>
          <header class="header-mobile header-mobile-3 d-block d-lg-none">
            <div class="header-mobile__bar">
              <div class="container-fluid">
                <div class="header-mobile-inner">
                  <a class="logo" href="index.html">
                    <img src="../public/images/icon/logo-bsch-rectangular.png" alt="Banco de Sangre" height="50%" />
                  </a>
                  <button class="hamburger hamburger--slider " type="button">
                    <span class="hamburger-box ">
                      <span class="hamburger-inner"></span>
                    </span>
                  </button>
                </div>
              </div>
            </div>
            <nav class="navbar-mobile">
              <div class="container-fluid">

                <div class="account-dropdown__footer bg-info">
                  <a href="../ajax/usuario.php?op=salir">
                    <i class="zmdi zmdi-power"></i>Salir</a>
                </div>
              </div>
            </nav>
          </header>
        <?php } ?>
        <!-- END HEADER MOBILE -->
      <?php }; ?>