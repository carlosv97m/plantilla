<?php require_once "../config/conexion.php";
require_once "../modelos/Atencion.php"; ?>



<!-- <div class="container w-100 h-100  d-flex flex-row bg-info"> -->
<div class="container row  ">
    <div class="col-8">
        <div class=" ">
            <img src="../public/images/icon/logo-bsch-rectangular.png" alt="" class="float-left p-1 w-25">
            <div class="datos float-right p-4">
                <b>
                    <date><?php echo  date("d") . " de " . date("m") . " del " . date("Y"); ?></date>
                </b>
            </div>
        </div>
        <div class="embed-responsive embed-responsive-16by9" id="player">
            <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/hTWKbfoikeg" title="YouTube video player" frameborder="0" id="video"></iframe> -->
        </div>
        <div class="anuncios">
            <marquee id="mensajeMonitor" class="bg-danger text-white" style="font-size: 26px; height: 55px;">
            </marquee>
        </div>

    </div>

    <div class="col-4 h-100">
        <div>
            <div class="col-12 ">
                <!-- DATA TABLE-->
                <div class=" h-100 ">
                    <table class="table table-borderless table-data3 ">
                        <thead>
                            <tr>
                                <th>Ticket</th>
                                <th>Consultorio</th>
                            </tr>
                        </thead>
                        <tbody id='contenido'>
                            <!-- CONTENIDO DE LA VISTA MONITOR -->
                        </tbody>
                    </table>
                </div>
                <!-- END DATA TABLE-->
            </div>
        </div>
    </div>
</div>
<script>
</script>
<!-- </div> -->