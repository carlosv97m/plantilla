<?php
ob_start();
session_start();
if (!isset($_SESSION["nombre"])) {
  header("Location: login.html");
} else {

  require "header.php";

?>
  <?php
  if (strlen(session_id()) < 1)
    session_start();
  ?>

  <!-- Contenido -->
  <section>
    <br>
    <div class="section__content section__content--p30">

      <div class="d-flex align-items-stretch h-100">

        <?php if ($_SESSION["cargo"] == "ticket") {
          require_once "ticket/ticket.php";
        }
        ?>
      </div>
      <?php
      if ($_SESSION["cargo"] == "monitor") {
        require_once "monitor/monitor.php";
      } else { ?>

        <div class="container-fluid ">
          <div class="row">
            <div class="col-xl-12">
              <!-- CONTENIDO CENTRAL-->

              <?php

              if ($_SESSION["cargo"] == "fidelizado") {
                require_once "vista_colas/fidelizado.php";
                echo '<input type="hidden" id="tipo_usuario" name="tipo_usuario" value="1">';
              }
              if ($_SESSION["cargo"] == "voluntario") {
                require_once "vista_colas/voluntario.php";
                echo '<input type="hidden" id="tipo_usuario" name="tipo_usuario" value="2">';
              }
              if ($_SESSION["cargo"] == "distribucion") {
                require_once "vista_colas/distribucion.php";
                echo '<input type="hidden" id="tipo_usuario" name="tipo_usuario" value="3">';
              }

              if ($_SESSION["cargo"] == "monitor") {
                require_once "monitor/monitor.php";
              }
              if ($_SESSION["cargo"] == "admin") {
                require_once "admin/admin.php";
              }
              ?>

            </div>

            <!-- END CONTENIDO CENTRAL-->
          </div>
      <?php
      }
    }

    require "footer.php";

      ?>
      <!-- Scripts de la pagina -->
      <?php
      if ($_SESSION["cargo"] == "fidelizado") { ?>
        <script src="../scripts/consultorio.js"></script>
      <?php }
      if ($_SESSION["cargo"] == "voluntario") { ?>
        <script src="../scripts/consultorio.js"></script>
      <?php }
      if ($_SESSION["cargo"] == "distribucion") { ?>
        <script src="../scripts/consultorio.js"></script>
      <?php }
      if ($_SESSION["cargo"] == "ticket") { ?>
        <script src="../public/vendor/axios/axios.min.js"></script>
        <script src="../scripts/ticket.js"></script>
      <?php }
      if ($_SESSION["cargo"] == "monitor") { ?>
        <script src="../public/vendor/axios/axios.min.js"></script>
        <script src="../scripts/monitor.js"></script>
      <?php }
      if ($_SESSION["cargo"] == "admin") { ?>
        <script src="../scripts/personal.js"></script>
        <script src="../scripts/video.js"></script>
        <script src="../scripts/mensaje.js"></script>
        <script src="../scripts/admin.js"></script>
      <?php } ?>

      <?php

      ob_end_flush();
      ?>