<!-- COPYRIGHT-->
<?php if ($_SESSION["cargo"] != "monitor" && $_SESSION["cargo"] != "ticket") { ?>
    <section class="p-t-60 p-b-20 ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>Copyright © 2021 Banco de Sangre. Todos los derechos reservados .</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END COPYRIGHT-->
    </div>


    </div>
<?php }; ?>


<!-- Jquery JS-->
<script src="../public/vendor/jquery/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="../public/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="../public/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<script src="../public/vendor/axios/axios.min.js"></script>

<!-- Vendor JS       -->

<script src="../public/vendor/animsition/animsition.min.js"></script>
<script src="../public/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>


<!-- Main JS-->
<script src="../public/js/main.js"></script>

<!--  -->

</body>

</html>