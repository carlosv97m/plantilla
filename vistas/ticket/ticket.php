<style>
    .row {
        padding-top: 20%;
    }

    h2 {
        color: white;
    }

    @media print {
        @page {
            margin: 0;
        }

        body {
            margin: 1.6cm;
        }
    }
</style>
<div class="col-md-12 ">
    <div class="row ">
        <div class="col-md-4 " aling="center">
            <button onclick="generar_ticket('Fidelizado')" class="btn btn-primary w-100 h-100">
                <img src="../public/images/icon/fidelizado.png" width="40%">
                <h2>
                    Donante Fidelizado
                </h2>

            </button>
        </div>
        <div class="col-md-4">
            <button onclick="generar_ticket('Voluntario')" class="btn btn-primary w-100 h-100">
                <img src="../public/images/icon/voluntario.png" width="40%">
                <h2>
                    Donante Voluntario
                </h2>
            </button>
        </div>
        <div class="col-md-4">
            <button onclick="generar_ticket('Distribucion')" class="btn btn-primary w-100 h-100">

                <img src="../public/images/icon/resultado-medico.png" width="40%">
                <h2>
                    Distribución
                </h2>
            </button>
        </div>
    </div>

</div>